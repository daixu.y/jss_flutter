import 'package:jss_flutter/generated/json/base/json_convert_content.dart';
import 'package:jss_flutter/bean/goods_response_entity.dart';

GoodsResponseEntity $GoodsResponseEntityFromJson(Map<String, dynamic> json) {
	final GoodsResponseEntity goodsResponseEntity = GoodsResponseEntity();
	final int? code = jsonConvert.convert<int>(json['code']);
	if (code != null) {
		goodsResponseEntity.code = code;
	}
	final List<GoodsResponseData>? data = jsonConvert.convertListNotNull<GoodsResponseData>(json['data']);
	if (data != null) {
		goodsResponseEntity.data = data;
	}
	final String? msg = jsonConvert.convert<String>(json['msg']);
	if (msg != null) {
		goodsResponseEntity.msg = msg;
	}
	final int? status = jsonConvert.convert<int>(json['status']);
	if (status != null) {
		goodsResponseEntity.status = status;
	}
	return goodsResponseEntity;
}

Map<String, dynamic> $GoodsResponseEntityToJson(GoodsResponseEntity entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['code'] = entity.code;
	data['data'] =  entity.data?.map((v) => v.toJson()).toList();
	data['msg'] = entity.msg;
	data['status'] = entity.status;
	return data;
}

GoodsResponseData $GoodsResponseDataFromJson(Map<String, dynamic> json) {
	final GoodsResponseData goodsResponseData = GoodsResponseData();
	final int? id = jsonConvert.convert<int>(json['id']);
	if (id != null) {
		goodsResponseData.id = id;
	}
	final String? name = jsonConvert.convert<String>(json['name']);
	if (name != null) {
		goodsResponseData.name = name;
	}
	final String? image = jsonConvert.convert<String>(json['image']);
	if (image != null) {
		goodsResponseData.image = image;
	}
	final String? price = jsonConvert.convert<String>(json['price']);
	if (price != null) {
		goodsResponseData.price = price;
	}
	final String? video = jsonConvert.convert<String>(json['video']);
	if (video != null) {
		goodsResponseData.video = video;
	}
	final int? showType = jsonConvert.convert<int>(json['show_type']);
	if (showType != null) {
		goodsResponseData.showType = showType;
	}
	final String? marketPrice = jsonConvert.convert<String>(json['market_price']);
	if (marketPrice != null) {
		goodsResponseData.marketPrice = marketPrice;
	}
	final int? status = jsonConvert.convert<int>(json['status']);
	if (status != null) {
		goodsResponseData.status = status;
	}
	final int? inventory = jsonConvert.convert<int>(json['inventory']);
	if (inventory != null) {
		goodsResponseData.inventory = inventory;
	}
	final int? sales = jsonConvert.convert<int>(json['sales']);
	if (sales != null) {
		goodsResponseData.sales = sales;
	}
	final int? freightTmpId = jsonConvert.convert<int>(json['freight_tmp_id']);
	if (freightTmpId != null) {
		goodsResponseData.freightTmpId = freightTmpId;
	}
	final int? type = jsonConvert.convert<int>(json['type']);
	if (type != null) {
		goodsResponseData.type = type;
	}
	final int? isAfhalen = jsonConvert.convert<int>(json['is_afhalen']);
	if (isAfhalen != null) {
		goodsResponseData.isAfhalen = isAfhalen;
	}
	final String? auditStatusText = jsonConvert.convert<String>(json['audit_status_text']);
	if (auditStatusText != null) {
		goodsResponseData.auditStatusText = auditStatusText;
	}
	final String? statusText = jsonConvert.convert<String>(json['status_text']);
	if (statusText != null) {
		goodsResponseData.statusText = statusText;
	}
	return goodsResponseData;
}

Map<String, dynamic> $GoodsResponseDataToJson(GoodsResponseData entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['id'] = entity.id;
	data['name'] = entity.name;
	data['image'] = entity.image;
	data['price'] = entity.price;
	data['video'] = entity.video;
	data['show_type'] = entity.showType;
	data['market_price'] = entity.marketPrice;
	data['status'] = entity.status;
	data['inventory'] = entity.inventory;
	data['sales'] = entity.sales;
	data['freight_tmp_id'] = entity.freightTmpId;
	data['type'] = entity.type;
	data['is_afhalen'] = entity.isAfhalen;
	data['audit_status_text'] = entity.auditStatusText;
	data['status_text'] = entity.statusText;
	return data;
}