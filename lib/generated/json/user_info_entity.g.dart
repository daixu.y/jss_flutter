import 'package:jss_flutter/generated/json/base/json_convert_content.dart';
import 'package:jss_flutter/bean/user_info_entity.dart';

UserInfoEntity $UserInfoEntityFromJson(Map<String, dynamic> json) {
	final UserInfoEntity userInfoEntity = UserInfoEntity();
	final int? code = jsonConvert.convert<int>(json['code']);
	if (code != null) {
		userInfoEntity.code = code;
	}
	final int? errorType = jsonConvert.convert<int>(json['error_type']);
	if (errorType != null) {
		userInfoEntity.errorType = errorType;
	}
	final UserInfoData? data = jsonConvert.convert<UserInfoData>(json['data']);
	if (data != null) {
		userInfoEntity.data = data;
	}
	final String? msg = jsonConvert.convert<String>(json['msg']);
	if (msg != null) {
		userInfoEntity.msg = msg;
	}
	final String? mess = jsonConvert.convert<String>(json['mess']);
	if (mess != null) {
		userInfoEntity.mess = mess;
	}
	final int? count = jsonConvert.convert<int>(json['count']);
	if (count != null) {
		userInfoEntity.count = count;
	}
	final int? status = jsonConvert.convert<int>(json['status']);
	if (status != null) {
		userInfoEntity.status = status;
	}
	return userInfoEntity;
}

Map<String, dynamic> $UserInfoEntityToJson(UserInfoEntity entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['code'] = entity.code;
	data['error_type'] = entity.errorType;
	data['data'] = entity.data?.toJson();
	data['msg'] = entity.msg;
	data['mess'] = entity.mess;
	data['count'] = entity.count;
	data['status'] = entity.status;
	return data;
}

UserInfoData $UserInfoDataFromJson(Map<String, dynamic> json) {
	final UserInfoData userInfoData = UserInfoData();
	final int? memberId = jsonConvert.convert<int>(json['member_id']);
	if (memberId != null) {
		userInfoData.memberId = memberId;
	}
	final String? nickname = jsonConvert.convert<String>(json['nickname']);
	if (nickname != null) {
		userInfoData.nickname = nickname;
	}
	final String? headimgurl = jsonConvert.convert<String>(json['headimgurl']);
	if (headimgurl != null) {
		userInfoData.headimgurl = headimgurl;
	}
	final String? phone = jsonConvert.convert<String>(json['phone']);
	if (phone != null) {
		userInfoData.phone = phone;
	}
	final int? creatorStatus = jsonConvert.convert<int>(json['creator_status']);
	if (creatorStatus != null) {
		userInfoData.creatorStatus = creatorStatus;
	}
	final int? cutCouponNum = jsonConvert.convert<int>(json['cut_coupon_num']);
	if (cutCouponNum != null) {
		userInfoData.cutCouponNum = cutCouponNum;
	}
	final int? cutCouponPastNum = jsonConvert.convert<int>(json['cut_coupon_past_num']);
	if (cutCouponPastNum != null) {
		userInfoData.cutCouponPastNum = cutCouponPastNum;
	}
	final String? jgName = jsonConvert.convert<String>(json['jg_name']);
	if (jgName != null) {
		userInfoData.jgName = jgName;
	}
	final int? credit = jsonConvert.convert<int>(json['credit']);
	if (credit != null) {
		userInfoData.credit = credit;
	}
	final int? cutLevel = jsonConvert.convert<int>(json['cut_level']);
	if (cutLevel != null) {
		userInfoData.cutLevel = cutLevel;
	}
	final String? money = jsonConvert.convert<String>(json['money']);
	if (money != null) {
		userInfoData.money = money;
	}
	final String? cutEarnings = jsonConvert.convert<String>(json['cut_earnings']);
	if (cutEarnings != null) {
		userInfoData.cutEarnings = cutEarnings;
	}
	final String? barterMoney = jsonConvert.convert<String>(json['barter_money']);
	if (barterMoney != null) {
		userInfoData.barterMoney = barterMoney;
	}
	final String? juMoney = jsonConvert.convert<String>(json['ju_money']);
	if (juMoney != null) {
		userInfoData.juMoney = juMoney;
	}
	final String? voucherMoney = jsonConvert.convert<String>(json['voucher_money']);
	if (voucherMoney != null) {
		userInfoData.voucherMoney = voucherMoney;
	}
	final String? shareMoney = jsonConvert.convert<String>(json['share_money']);
	if (shareMoney != null) {
		userInfoData.shareMoney = shareMoney;
	}
	final String? shareMoneyTotal = jsonConvert.convert<String>(json['share_money_total']);
	if (shareMoneyTotal != null) {
		userInfoData.shareMoneyTotal = shareMoneyTotal;
	}
	final String? shareBarterTotal = jsonConvert.convert<String>(json['share_barter_total']);
	if (shareBarterTotal != null) {
		userInfoData.shareBarterTotal = shareBarterTotal;
	}
	final int? shareCutTotal = jsonConvert.convert<int>(json['share_cut_total']);
	if (shareCutTotal != null) {
		userInfoData.shareCutTotal = shareCutTotal;
	}
	final int? shareCutCouponTotal = jsonConvert.convert<int>(json['share_cut_coupon_total']);
	if (shareCutCouponTotal != null) {
		userInfoData.shareCutCouponTotal = shareCutCouponTotal;
	}
	final int? yesterdayIncome = jsonConvert.convert<int>(json['yesterday_income']);
	if (yesterdayIncome != null) {
		userInfoData.yesterdayIncome = yesterdayIncome;
	}
	final int? shopId = jsonConvert.convert<int>(json['shop_id']);
	if (shopId != null) {
		userInfoData.shopId = shopId;
	}
	final int? sex = jsonConvert.convert<int>(json['sex']);
	if (sex != null) {
		userInfoData.sex = sex;
	}
	final int? issetPayPassword = jsonConvert.convert<int>(json['isset_pay_password']);
	if (issetPayPassword != null) {
		userInfoData.issetPayPassword = issetPayPassword;
	}
	final String? birthday = jsonConvert.convert<String>(json['birthday']);
	if (birthday != null) {
		userInfoData.birthday = birthday;
	}
	final String? signature = jsonConvert.convert<String>(json['signature']);
	if (signature != null) {
		userInfoData.signature = signature;
	}
	final int? master = jsonConvert.convert<int>(json['master']);
	if (master != null) {
		userInfoData.master = master;
	}
	final String? inviteCode = jsonConvert.convert<String>(json['invite_code']);
	if (inviteCode != null) {
		userInfoData.inviteCode = inviteCode;
	}
	final int? supermarket = jsonConvert.convert<int>(json['supermarket']);
	if (supermarket != null) {
		userInfoData.supermarket = supermarket;
	}
	final int? planNum = jsonConvert.convert<int>(json['plan_num']);
	if (planNum != null) {
		userInfoData.planNum = planNum;
	}
	final int? isShopShare = jsonConvert.convert<int>(json['is_shop_share']);
	if (isShopShare != null) {
		userInfoData.isShopShare = isShopShare;
	}
	return userInfoData;
}

Map<String, dynamic> $UserInfoDataToJson(UserInfoData entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['member_id'] = entity.memberId;
	data['nickname'] = entity.nickname;
	data['headimgurl'] = entity.headimgurl;
	data['phone'] = entity.phone;
	data['creator_status'] = entity.creatorStatus;
	data['cut_coupon_num'] = entity.cutCouponNum;
	data['cut_coupon_past_num'] = entity.cutCouponPastNum;
	data['jg_name'] = entity.jgName;
	data['credit'] = entity.credit;
	data['cut_level'] = entity.cutLevel;
	data['money'] = entity.money;
	data['cut_earnings'] = entity.cutEarnings;
	data['barter_money'] = entity.barterMoney;
	data['ju_money'] = entity.juMoney;
	data['voucher_money'] = entity.voucherMoney;
	data['share_money'] = entity.shareMoney;
	data['share_money_total'] = entity.shareMoneyTotal;
	data['share_barter_total'] = entity.shareBarterTotal;
	data['share_cut_total'] = entity.shareCutTotal;
	data['share_cut_coupon_total'] = entity.shareCutCouponTotal;
	data['yesterday_income'] = entity.yesterdayIncome;
	data['shop_id'] = entity.shopId;
	data['sex'] = entity.sex;
	data['isset_pay_password'] = entity.issetPayPassword;
	data['birthday'] = entity.birthday;
	data['signature'] = entity.signature;
	data['master'] = entity.master;
	data['invite_code'] = entity.inviteCode;
	data['supermarket'] = entity.supermarket;
	data['plan_num'] = entity.planNum;
	data['is_shop_share'] = entity.isShopShare;
	return data;
}