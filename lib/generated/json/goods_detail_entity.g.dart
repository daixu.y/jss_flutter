import 'package:jss_flutter/generated/json/base/json_convert_content.dart';
import 'package:jss_flutter/bean/goods_detail_entity.dart';

GoodsDetailEntity $GoodsDetailEntityFromJson(Map<String, dynamic> json) {
	final GoodsDetailEntity goodsDetailEntity = GoodsDetailEntity();
	final int? code = jsonConvert.convert<int>(json['code']);
	if (code != null) {
		goodsDetailEntity.code = code;
	}
	final GoodsDetailData? data = jsonConvert.convert<GoodsDetailData>(json['data']);
	if (data != null) {
		goodsDetailEntity.data = data;
	}
	final String? msg = jsonConvert.convert<String>(json['msg']);
	if (msg != null) {
		goodsDetailEntity.msg = msg;
	}
	final int? status = jsonConvert.convert<int>(json['status']);
	if (status != null) {
		goodsDetailEntity.status = status;
	}
	return goodsDetailEntity;
}

Map<String, dynamic> $GoodsDetailEntityToJson(GoodsDetailEntity entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['code'] = entity.code;
	data['data'] = entity.data?.toJson();
	data['msg'] = entity.msg;
	data['status'] = entity.status;
	return data;
}

GoodsDetailData $GoodsDetailDataFromJson(Map<String, dynamic> json) {
	final GoodsDetailData goodsDetailData = GoodsDetailData();
	final int? id = jsonConvert.convert<int>(json['id']);
	if (id != null) {
		goodsDetailData.id = id;
	}
	final int? categoryIdOne = jsonConvert.convert<int>(json['category_id_one']);
	if (categoryIdOne != null) {
		goodsDetailData.categoryIdOne = categoryIdOne;
	}
	final int? categoryIdTwo = jsonConvert.convert<int>(json['category_id_two']);
	if (categoryIdTwo != null) {
		goodsDetailData.categoryIdTwo = categoryIdTwo;
	}
	final int? categoryIdThree = jsonConvert.convert<int>(json['category_id_three']);
	if (categoryIdThree != null) {
		goodsDetailData.categoryIdThree = categoryIdThree;
	}
	final int? shopCategoryIdOne = jsonConvert.convert<int>(json['shop_category_id_one']);
	if (shopCategoryIdOne != null) {
		goodsDetailData.shopCategoryIdOne = shopCategoryIdOne;
	}
	final int? shopCategoryIdTwo = jsonConvert.convert<int>(json['shop_category_id_two']);
	if (shopCategoryIdTwo != null) {
		goodsDetailData.shopCategoryIdTwo = shopCategoryIdTwo;
	}
	final int? shopCategoryIdThree = jsonConvert.convert<int>(json['shop_category_id_three']);
	if (shopCategoryIdThree != null) {
		goodsDetailData.shopCategoryIdThree = shopCategoryIdThree;
	}
	final String? name = jsonConvert.convert<String>(json['name']);
	if (name != null) {
		goodsDetailData.name = name;
	}
	final String? image = jsonConvert.convert<String>(json['image']);
	if (image != null) {
		goodsDetailData.image = image;
	}
	final String? video = jsonConvert.convert<String>(json['video']);
	if (video != null) {
		goodsDetailData.video = video;
	}
	final String? desc = jsonConvert.convert<String>(json['desc']);
	if (desc != null) {
		goodsDetailData.desc = desc;
	}
	final int? showType = jsonConvert.convert<int>(json['show_type']);
	if (showType != null) {
		goodsDetailData.showType = showType;
	}
	final String? price = jsonConvert.convert<String>(json['price']);
	if (price != null) {
		goodsDetailData.price = price;
	}
	final String? marketPrice = jsonConvert.convert<String>(json['market_price']);
	if (marketPrice != null) {
		goodsDetailData.marketPrice = marketPrice;
	}
	final int? status = jsonConvert.convert<int>(json['status']);
	if (status != null) {
		goodsDetailData.status = status;
	}
	final int? auditStatus = jsonConvert.convert<int>(json['audit_status']);
	if (auditStatus != null) {
		goodsDetailData.auditStatus = auditStatus;
	}
	final String? auditRemark = jsonConvert.convert<String>(json['audit_remark']);
	if (auditRemark != null) {
		goodsDetailData.auditRemark = auditRemark;
	}
	final int? sales = jsonConvert.convert<int>(json['sales']);
	if (sales != null) {
		goodsDetailData.sales = sales;
	}
	final int? inventory = jsonConvert.convert<int>(json['inventory']);
	if (inventory != null) {
		goodsDetailData.inventory = inventory;
	}
	final int? type = jsonConvert.convert<int>(json['type']);
	if (type != null) {
		goodsDetailData.type = type;
	}
	final String? shareRatio = jsonConvert.convert<String>(json['share_ratio']);
	if (shareRatio != null) {
		goodsDetailData.shareRatio = shareRatio;
	}
	final int? sort = jsonConvert.convert<int>(json['sort']);
	if (sort != null) {
		goodsDetailData.sort = sort;
	}
	final int? cityId = jsonConvert.convert<int>(json['city_id']);
	if (cityId != null) {
		goodsDetailData.cityId = cityId;
	}
	final int? shopId = jsonConvert.convert<int>(json['shop_id']);
	if (shopId != null) {
		goodsDetailData.shopId = shopId;
	}
	final int? isSync = jsonConvert.convert<int>(json['is_sync']);
	if (isSync != null) {
		goodsDetailData.isSync = isSync;
	}
	final int? freightTmpId = jsonConvert.convert<int>(json['freight_tmp_id']);
	if (freightTmpId != null) {
		goodsDetailData.freightTmpId = freightTmpId;
	}
	final int? isAfhalen = jsonConvert.convert<int>(json['is_afhalen']);
	if (isAfhalen != null) {
		goodsDetailData.isAfhalen = isAfhalen;
	}
	final String? createdAt = jsonConvert.convert<String>(json['created_at']);
	if (createdAt != null) {
		goodsDetailData.createdAt = createdAt;
	}
	final String? updatedAt = jsonConvert.convert<String>(json['updated_at']);
	if (updatedAt != null) {
		goodsDetailData.updatedAt = updatedAt;
	}
	final String? auditStatusText = jsonConvert.convert<String>(json['audit_status_text']);
	if (auditStatusText != null) {
		goodsDetailData.auditStatusText = auditStatusText;
	}
	final String? statusText = jsonConvert.convert<String>(json['status_text']);
	if (statusText != null) {
		goodsDetailData.statusText = statusText;
	}
	final List<GoodsDetailDataImages>? images = jsonConvert.convertListNotNull<GoodsDetailDataImages>(json['images']);
	if (images != null) {
		goodsDetailData.images = images;
	}
	final List<GoodsDetailDataSkus>? skus = jsonConvert.convertListNotNull<GoodsDetailDataSkus>(json['skus']);
	if (skus != null) {
		goodsDetailData.skus = skus;
	}
	final GoodsDetailDataShopInfo? shopInfo = jsonConvert.convert<GoodsDetailDataShopInfo>(json['shop_info']);
	if (shopInfo != null) {
		goodsDetailData.shopInfo = shopInfo;
	}
	final int? isCollect = jsonConvert.convert<int>(json['is_collect']);
	if (isCollect != null) {
		goodsDetailData.isCollect = isCollect;
	}
	final List<GoodsDetailDataPlanList>? planList = jsonConvert.convertListNotNull<GoodsDetailDataPlanList>(json['plan_list']);
	if (planList != null) {
		goodsDetailData.planList = planList;
	}
	final int? distance = jsonConvert.convert<int>(json['distance']);
	if (distance != null) {
		goodsDetailData.distance = distance;
	}
	final String? categoryNameOne = jsonConvert.convert<String>(json['category_name_one']);
	if (categoryNameOne != null) {
		goodsDetailData.categoryNameOne = categoryNameOne;
	}
	final String? categoryNameTwo = jsonConvert.convert<String>(json['category_name_two']);
	if (categoryNameTwo != null) {
		goodsDetailData.categoryNameTwo = categoryNameTwo;
	}
	final String? categoryNameThree = jsonConvert.convert<String>(json['category_name_three']);
	if (categoryNameThree != null) {
		goodsDetailData.categoryNameThree = categoryNameThree;
	}
	final int? shareShow = jsonConvert.convert<int>(json['share_show']);
	if (shareShow != null) {
		goodsDetailData.shareShow = shareShow;
	}
	return goodsDetailData;
}

Map<String, dynamic> $GoodsDetailDataToJson(GoodsDetailData entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['id'] = entity.id;
	data['category_id_one'] = entity.categoryIdOne;
	data['category_id_two'] = entity.categoryIdTwo;
	data['category_id_three'] = entity.categoryIdThree;
	data['shop_category_id_one'] = entity.shopCategoryIdOne;
	data['shop_category_id_two'] = entity.shopCategoryIdTwo;
	data['shop_category_id_three'] = entity.shopCategoryIdThree;
	data['name'] = entity.name;
	data['image'] = entity.image;
	data['video'] = entity.video;
	data['desc'] = entity.desc;
	data['show_type'] = entity.showType;
	data['price'] = entity.price;
	data['market_price'] = entity.marketPrice;
	data['status'] = entity.status;
	data['audit_status'] = entity.auditStatus;
	data['audit_remark'] = entity.auditRemark;
	data['sales'] = entity.sales;
	data['inventory'] = entity.inventory;
	data['type'] = entity.type;
	data['share_ratio'] = entity.shareRatio;
	data['sort'] = entity.sort;
	data['city_id'] = entity.cityId;
	data['shop_id'] = entity.shopId;
	data['is_sync'] = entity.isSync;
	data['freight_tmp_id'] = entity.freightTmpId;
	data['is_afhalen'] = entity.isAfhalen;
	data['created_at'] = entity.createdAt;
	data['updated_at'] = entity.updatedAt;
	data['audit_status_text'] = entity.auditStatusText;
	data['status_text'] = entity.statusText;
	data['images'] =  entity.images?.map((v) => v.toJson()).toList();
	data['skus'] =  entity.skus?.map((v) => v.toJson()).toList();
	data['shop_info'] = entity.shopInfo?.toJson();
	data['is_collect'] = entity.isCollect;
	data['plan_list'] =  entity.planList?.map((v) => v.toJson()).toList();
	data['distance'] = entity.distance;
	data['category_name_one'] = entity.categoryNameOne;
	data['category_name_two'] = entity.categoryNameTwo;
	data['category_name_three'] = entity.categoryNameThree;
	data['share_show'] = entity.shareShow;
	return data;
}

GoodsDetailDataImages $GoodsDetailDataImagesFromJson(Map<String, dynamic> json) {
	final GoodsDetailDataImages goodsDetailDataImages = GoodsDetailDataImages();
	final String? image = jsonConvert.convert<String>(json['image']);
	if (image != null) {
		goodsDetailDataImages.image = image;
	}
	final int? type = jsonConvert.convert<int>(json['type']);
	if (type != null) {
		goodsDetailDataImages.type = type;
	}
	return goodsDetailDataImages;
}

Map<String, dynamic> $GoodsDetailDataImagesToJson(GoodsDetailDataImages entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['image'] = entity.image;
	data['type'] = entity.type;
	return data;
}

GoodsDetailDataSkus $GoodsDetailDataSkusFromJson(Map<String, dynamic> json) {
	final GoodsDetailDataSkus goodsDetailDataSkus = GoodsDetailDataSkus();
	final int? id = jsonConvert.convert<int>(json['id']);
	if (id != null) {
		goodsDetailDataSkus.id = id;
	}
	final String? price = jsonConvert.convert<String>(json['price']);
	if (price != null) {
		goodsDetailDataSkus.price = price;
	}
	final String? image = jsonConvert.convert<String>(json['image']);
	if (image != null) {
		goodsDetailDataSkus.image = image;
	}
	final String? name = jsonConvert.convert<String>(json['name']);
	if (name != null) {
		goodsDetailDataSkus.name = name;
	}
	final int? inventory = jsonConvert.convert<int>(json['inventory']);
	if (inventory != null) {
		goodsDetailDataSkus.inventory = inventory;
	}
	final int? isPermanentValidity = jsonConvert.convert<int>(json['is_permanent_validity']);
	if (isPermanentValidity != null) {
		goodsDetailDataSkus.isPermanentValidity = isPermanentValidity;
	}
	final String? endTime = jsonConvert.convert<String>(json['end_time']);
	if (endTime != null) {
		goodsDetailDataSkus.endTime = endTime;
	}
	final String? cutTotalCommission = jsonConvert.convert<String>(json['cut_total_commission']);
	if (cutTotalCommission != null) {
		goodsDetailDataSkus.cutTotalCommission = cutTotalCommission;
	}
	final String? attrValueIds = jsonConvert.convert<String>(json['attr_value_ids']);
	if (attrValueIds != null) {
		goodsDetailDataSkus.attrValueIds = attrValueIds;
	}
	final int? cutTime = jsonConvert.convert<int>(json['cut_time']);
	if (cutTime != null) {
		goodsDetailDataSkus.cutTime = cutTime;
	}
	final int? cutPeople = jsonConvert.convert<int>(json['cut_people']);
	if (cutPeople != null) {
		goodsDetailDataSkus.cutPeople = cutPeople;
	}
	final int? giveCutCouponNum = jsonConvert.convert<int>(json['give_cut_coupon_num']);
	if (giveCutCouponNum != null) {
		goodsDetailDataSkus.giveCutCouponNum = giveCutCouponNum;
	}
	return goodsDetailDataSkus;
}

Map<String, dynamic> $GoodsDetailDataSkusToJson(GoodsDetailDataSkus entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['id'] = entity.id;
	data['price'] = entity.price;
	data['image'] = entity.image;
	data['name'] = entity.name;
	data['inventory'] = entity.inventory;
	data['is_permanent_validity'] = entity.isPermanentValidity;
	data['end_time'] = entity.endTime;
	data['cut_total_commission'] = entity.cutTotalCommission;
	data['attr_value_ids'] = entity.attrValueIds;
	data['cut_time'] = entity.cutTime;
	data['cut_people'] = entity.cutPeople;
	data['give_cut_coupon_num'] = entity.giveCutCouponNum;
	return data;
}

GoodsDetailDataShopInfo $GoodsDetailDataShopInfoFromJson(Map<String, dynamic> json) {
	final GoodsDetailDataShopInfo goodsDetailDataShopInfo = GoodsDetailDataShopInfo();
	final String? logo = jsonConvert.convert<String>(json['logo']);
	if (logo != null) {
		goodsDetailDataShopInfo.logo = logo;
	}
	final double? lat = jsonConvert.convert<double>(json['lat']);
	if (lat != null) {
		goodsDetailDataShopInfo.lat = lat;
	}
	final double? lng = jsonConvert.convert<double>(json['lng']);
	if (lng != null) {
		goodsDetailDataShopInfo.lng = lng;
	}
	final int? memberId = jsonConvert.convert<int>(json['member_id']);
	if (memberId != null) {
		goodsDetailDataShopInfo.memberId = memberId;
	}
	final String? name = jsonConvert.convert<String>(json['name']);
	if (name != null) {
		goodsDetailDataShopInfo.name = name;
	}
	final String? address = jsonConvert.convert<String>(json['address']);
	if (address != null) {
		goodsDetailDataShopInfo.address = address;
	}
	final String? regionInfo = jsonConvert.convert<String>(json['region_info']);
	if (regionInfo != null) {
		goodsDetailDataShopInfo.regionInfo = regionInfo;
	}
	final String? phone = jsonConvert.convert<String>(json['phone']);
	if (phone != null) {
		goodsDetailDataShopInfo.phone = phone;
	}
	final int? isLive = jsonConvert.convert<int>(json['is_live']);
	if (isLive != null) {
		goodsDetailDataShopInfo.isLive = isLive;
	}
	final String? auditStatusText = jsonConvert.convert<String>(json['audit_status_text']);
	if (auditStatusText != null) {
		goodsDetailDataShopInfo.auditStatusText = auditStatusText;
	}
	final String? earnestStatusText = jsonConvert.convert<String>(json['earnest_status_text']);
	if (earnestStatusText != null) {
		goodsDetailDataShopInfo.earnestStatusText = earnestStatusText;
	}
	final String? statusText = jsonConvert.convert<String>(json['status_text']);
	if (statusText != null) {
		goodsDetailDataShopInfo.statusText = statusText;
	}
	return goodsDetailDataShopInfo;
}

Map<String, dynamic> $GoodsDetailDataShopInfoToJson(GoodsDetailDataShopInfo entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['logo'] = entity.logo;
	data['lat'] = entity.lat;
	data['lng'] = entity.lng;
	data['member_id'] = entity.memberId;
	data['name'] = entity.name;
	data['address'] = entity.address;
	data['region_info'] = entity.regionInfo;
	data['phone'] = entity.phone;
	data['is_live'] = entity.isLive;
	data['audit_status_text'] = entity.auditStatusText;
	data['earnest_status_text'] = entity.earnestStatusText;
	data['status_text'] = entity.statusText;
	return data;
}

GoodsDetailDataPlanList $GoodsDetailDataPlanListFromJson(Map<String, dynamic> json) {
	final GoodsDetailDataPlanList goodsDetailDataPlanList = GoodsDetailDataPlanList();
	final String? id = jsonConvert.convert<String>(json['id']);
	if (id != null) {
		goodsDetailDataPlanList.id = id;
	}
	final String? shopId = jsonConvert.convert<String>(json['shop_id']);
	if (shopId != null) {
		goodsDetailDataPlanList.shopId = shopId;
	}
	final int? online = jsonConvert.convert<int>(json['online']);
	if (online != null) {
		goodsDetailDataPlanList.online = online;
	}
	final int? type = jsonConvert.convert<int>(json['type']);
	if (type != null) {
		goodsDetailDataPlanList.type = type;
	}
	final String? title = jsonConvert.convert<String>(json['title']);
	if (title != null) {
		goodsDetailDataPlanList.title = title;
	}
	final String? amount = jsonConvert.convert<String>(json['amount']);
	if (amount != null) {
		goodsDetailDataPlanList.amount = amount;
	}
	final int? limitAmount = jsonConvert.convert<int>(json['limit_amount']);
	if (limitAmount != null) {
		goodsDetailDataPlanList.limitAmount = limitAmount;
	}
	final int? discount = jsonConvert.convert<int>(json['discount']);
	if (discount != null) {
		goodsDetailDataPlanList.discount = discount;
	}
	final String? goodsIds = jsonConvert.convert<String>(json['goods_ids']);
	if (goodsIds != null) {
		goodsDetailDataPlanList.goodsIds = goodsIds;
	}
	final int? number = jsonConvert.convert<int>(json['number']);
	if (number != null) {
		goodsDetailDataPlanList.number = number;
	}
	final int? getNumber = jsonConvert.convert<int>(json['get_number']);
	if (getNumber != null) {
		goodsDetailDataPlanList.getNumber = getNumber;
	}
	final int? expireNumber = jsonConvert.convert<int>(json['expire_number']);
	if (expireNumber != null) {
		goodsDetailDataPlanList.expireNumber = expireNumber;
	}
	final int? useNumber = jsonConvert.convert<int>(json['use_number']);
	if (useNumber != null) {
		goodsDetailDataPlanList.useNumber = useNumber;
	}
	final int? status = jsonConvert.convert<int>(json['status']);
	if (status != null) {
		goodsDetailDataPlanList.status = status;
	}
	final int? isNew = jsonConvert.convert<int>(json['is_new']);
	if (isNew != null) {
		goodsDetailDataPlanList.isNew = isNew;
	}
	final int? startTime = jsonConvert.convert<int>(json['start_time']);
	if (startTime != null) {
		goodsDetailDataPlanList.startTime = startTime;
	}
	final int? endTime = jsonConvert.convert<int>(json['end_time']);
	if (endTime != null) {
		goodsDetailDataPlanList.endTime = endTime;
	}
	final String? createdAt = jsonConvert.convert<String>(json['created_at']);
	if (createdAt != null) {
		goodsDetailDataPlanList.createdAt = createdAt;
	}
	final String? updatedAt = jsonConvert.convert<String>(json['updated_at']);
	if (updatedAt != null) {
		goodsDetailDataPlanList.updatedAt = updatedAt;
	}
	final int? isGet = jsonConvert.convert<int>(json['is_get']);
	if (isGet != null) {
		goodsDetailDataPlanList.isGet = isGet;
	}
	return goodsDetailDataPlanList;
}

Map<String, dynamic> $GoodsDetailDataPlanListToJson(GoodsDetailDataPlanList entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['id'] = entity.id;
	data['shop_id'] = entity.shopId;
	data['online'] = entity.online;
	data['type'] = entity.type;
	data['title'] = entity.title;
	data['amount'] = entity.amount;
	data['limit_amount'] = entity.limitAmount;
	data['discount'] = entity.discount;
	data['goods_ids'] = entity.goodsIds;
	data['number'] = entity.number;
	data['get_number'] = entity.getNumber;
	data['expire_number'] = entity.expireNumber;
	data['use_number'] = entity.useNumber;
	data['status'] = entity.status;
	data['is_new'] = entity.isNew;
	data['start_time'] = entity.startTime;
	data['end_time'] = entity.endTime;
	data['created_at'] = entity.createdAt;
	data['updated_at'] = entity.updatedAt;
	data['is_get'] = entity.isGet;
	return data;
}