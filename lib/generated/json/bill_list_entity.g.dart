import 'package:jss_flutter/generated/json/base/json_convert_content.dart';
import 'package:jss_flutter/bean/bill_list_entity.dart';

BillListEntity $BillListEntityFromJson(Map<String, dynamic> json) {
	final BillListEntity billListEntity = BillListEntity();
	final int? code = jsonConvert.convert<int>(json['code']);
	if (code != null) {
		billListEntity.code = code;
	}
	final List<BillListData>? data = jsonConvert.convertListNotNull<BillListData>(json['data']);
	if (data != null) {
		billListEntity.data = data;
	}
	final String? msg = jsonConvert.convert<String>(json['msg']);
	if (msg != null) {
		billListEntity.msg = msg;
	}
	final int? status = jsonConvert.convert<int>(json['status']);
	if (status != null) {
		billListEntity.status = status;
	}
	return billListEntity;
}

Map<String, dynamic> $BillListEntityToJson(BillListEntity entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['code'] = entity.code;
	data['data'] =  entity.data?.map((v) => v.toJson()).toList();
	data['msg'] = entity.msg;
	data['status'] = entity.status;
	return data;
}

BillListData $BillListDataFromJson(Map<String, dynamic> json) {
	final BillListData billListData = BillListData();
	final int? id = jsonConvert.convert<int>(json['id']);
	if (id != null) {
		billListData.id = id;
	}
	final String? money = jsonConvert.convert<String>(json['money']);
	if (money != null) {
		billListData.money = money;
	}
	final String? typeText = jsonConvert.convert<String>(json['type_text']);
	if (typeText != null) {
		billListData.typeText = typeText;
	}
	final String? surplusMoney = jsonConvert.convert<String>(json['surplus_money']);
	if (surplusMoney != null) {
		billListData.surplusMoney = surplusMoney;
	}
	final String? oldMoney = jsonConvert.convert<String>(json['old_money']);
	if (oldMoney != null) {
		billListData.oldMoney = oldMoney;
	}
	final String? newMoney = jsonConvert.convert<String>(json['new_money']);
	if (newMoney != null) {
		billListData.newMoney = newMoney;
	}
	final String? remark = jsonConvert.convert<String>(json['remark']);
	if (remark != null) {
		billListData.remark = remark;
	}
	final String? createdAt = jsonConvert.convert<String>(json['created_at']);
	if (createdAt != null) {
		billListData.createdAt = createdAt;
	}
	return billListData;
}

Map<String, dynamic> $BillListDataToJson(BillListData entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['id'] = entity.id;
	data['money'] = entity.money;
	data['type_text'] = entity.typeText;
	data['surplus_money'] = entity.surplusMoney;
	data['old_money'] = entity.oldMoney;
	data['new_money'] = entity.newMoney;
	data['remark'] = entity.remark;
	data['created_at'] = entity.createdAt;
	return data;
}