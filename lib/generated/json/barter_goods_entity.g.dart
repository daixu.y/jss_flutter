import 'package:jss_flutter/generated/json/base/json_convert_content.dart';
import 'package:jss_flutter/bean/barter_goods_entity.dart';

BarterGoodsEntity $BarterGoodsEntityFromJson(Map<String, dynamic> json) {
	final BarterGoodsEntity barterGoodsEntity = BarterGoodsEntity();
	final int? code = jsonConvert.convert<int>(json['code']);
	if (code != null) {
		barterGoodsEntity.code = code;
	}
	final List<BarterGoodsData>? data = jsonConvert.convertListNotNull<BarterGoodsData>(json['data']);
	if (data != null) {
		barterGoodsEntity.data = data;
	}
	final String? msg = jsonConvert.convert<String>(json['msg']);
	if (msg != null) {
		barterGoodsEntity.msg = msg;
	}
	final int? status = jsonConvert.convert<int>(json['status']);
	if (status != null) {
		barterGoodsEntity.status = status;
	}
	return barterGoodsEntity;
}

Map<String, dynamic> $BarterGoodsEntityToJson(BarterGoodsEntity entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['code'] = entity.code;
	data['data'] =  entity.data?.map((v) => v.toJson()).toList();
	data['msg'] = entity.msg;
	data['status'] = entity.status;
	return data;
}

BarterGoodsData $BarterGoodsDataFromJson(Map<String, dynamic> json) {
	final BarterGoodsData barterGoodsData = BarterGoodsData();
	final int? id = jsonConvert.convert<int>(json['id']);
	if (id != null) {
		barterGoodsData.id = id;
	}
	final int? activityId = jsonConvert.convert<int>(json['activity_id']);
	if (activityId != null) {
		barterGoodsData.activityId = activityId;
	}
	final int? goodsId = jsonConvert.convert<int>(json['goods_id']);
	if (goodsId != null) {
		barterGoodsData.goodsId = goodsId;
	}
	final int? isAfhalen = jsonConvert.convert<int>(json['is_afhalen']);
	if (isAfhalen != null) {
		barterGoodsData.isAfhalen = isAfhalen;
	}
	final String? price = jsonConvert.convert<String>(json['price']);
	if (price != null) {
		barterGoodsData.price = price;
	}
	final int? goodsSales = jsonConvert.convert<int>(json['goods_sales']);
	if (goodsSales != null) {
		barterGoodsData.goodsSales = goodsSales;
	}
	final int? showType = jsonConvert.convert<int>(json['show_type']);
	if (showType != null) {
		barterGoodsData.showType = showType;
	}
	final String? goodsName = jsonConvert.convert<String>(json['goods_name']);
	if (goodsName != null) {
		barterGoodsData.goodsName = goodsName;
	}
	final String? goodsImage = jsonConvert.convert<String>(json['goods_image']);
	if (goodsImage != null) {
		barterGoodsData.goodsImage = goodsImage;
	}
	final String? shopName = jsonConvert.convert<String>(json['shop_name']);
	if (shopName != null) {
		barterGoodsData.shopName = shopName;
	}
	final String? shopLogo = jsonConvert.convert<String>(json['shop_logo']);
	if (shopLogo != null) {
		barterGoodsData.shopLogo = shopLogo;
	}
	final double? shopLng = jsonConvert.convert<double>(json['shop_lng']);
	if (shopLng != null) {
		barterGoodsData.shopLng = shopLng;
	}
	final double? shopLat = jsonConvert.convert<double>(json['shop_lat']);
	if (shopLat != null) {
		barterGoodsData.shopLat = shopLat;
	}
	final double? distance = jsonConvert.convert<double>(json['distance']);
	if (distance != null) {
		barterGoodsData.distance = distance;
	}
	final String? inventory = jsonConvert.convert<String>(json['inventory']);
	if (inventory != null) {
		barterGoodsData.inventory = inventory;
	}
	final String? totalInventory = jsonConvert.convert<String>(json['total_inventory']);
	if (totalInventory != null) {
		barterGoodsData.totalInventory = totalInventory;
	}
	return barterGoodsData;
}

Map<String, dynamic> $BarterGoodsDataToJson(BarterGoodsData entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['id'] = entity.id;
	data['activity_id'] = entity.activityId;
	data['goods_id'] = entity.goodsId;
	data['is_afhalen'] = entity.isAfhalen;
	data['price'] = entity.price;
	data['goods_sales'] = entity.goodsSales;
	data['show_type'] = entity.showType;
	data['goods_name'] = entity.goodsName;
	data['goods_image'] = entity.goodsImage;
	data['shop_name'] = entity.shopName;
	data['shop_logo'] = entity.shopLogo;
	data['shop_lng'] = entity.shopLng;
	data['shop_lat'] = entity.shopLat;
	data['distance'] = entity.distance;
	data['inventory'] = entity.inventory;
	data['total_inventory'] = entity.totalInventory;
	return data;
}