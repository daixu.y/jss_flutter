import 'package:jss_flutter/generated/json/base/json_convert_content.dart';
import 'package:jss_flutter/bean/order_list_entity.dart';

OrderListEntity $OrderListEntityFromJson(Map<String, dynamic> json) {
	final OrderListEntity orderListEntity = OrderListEntity();
	final int? code = jsonConvert.convert<int>(json['code']);
	if (code != null) {
		orderListEntity.code = code;
	}
	final List<OrderListData>? data = jsonConvert.convertListNotNull<OrderListData>(json['data']);
	if (data != null) {
		orderListEntity.data = data;
	}
	final String? msg = jsonConvert.convert<String>(json['msg']);
	if (msg != null) {
		orderListEntity.msg = msg;
	}
	final int? status = jsonConvert.convert<int>(json['status']);
	if (status != null) {
		orderListEntity.status = status;
	}
	return orderListEntity;
}

Map<String, dynamic> $OrderListEntityToJson(OrderListEntity entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['code'] = entity.code;
	data['data'] =  entity.data?.map((v) => v.toJson()).toList();
	data['msg'] = entity.msg;
	data['status'] = entity.status;
	return data;
}

OrderListData $OrderListDataFromJson(Map<String, dynamic> json) {
	final OrderListData orderListData = OrderListData();
	final int? id = jsonConvert.convert<int>(json['id']);
	if (id != null) {
		orderListData.id = id;
	}
	final String? orderNo = jsonConvert.convert<String>(json['order_no']);
	if (orderNo != null) {
		orderListData.orderNo = orderNo;
	}
	final String? time = jsonConvert.convert<String>(json['time']);
	if (time != null) {
		orderListData.time = time;
	}
	final int? status = jsonConvert.convert<int>(json['status']);
	if (status != null) {
		orderListData.status = status;
	}
	final String? statusText = jsonConvert.convert<String>(json['status_text']);
	if (statusText != null) {
		orderListData.statusText = statusText;
	}
	final int? cutEnd = jsonConvert.convert<int>(json['cut_end']);
	if (cutEnd != null) {
		orderListData.cutEnd = cutEnd;
	}
	final int? afterSaleStatus = jsonConvert.convert<int>(json['after_sale_status']);
	if (afterSaleStatus != null) {
		orderListData.afterSaleStatus = afterSaleStatus;
	}
	final int? commentStatus = jsonConvert.convert<int>(json['comment_status']);
	if (commentStatus != null) {
		orderListData.commentStatus = commentStatus;
	}
	final int? type = jsonConvert.convert<int>(json['type']);
	if (type != null) {
		orderListData.type = type;
	}
	final String? unpaid = jsonConvert.convert<String>(json['unpaid']);
	if (unpaid != null) {
		orderListData.unpaid = unpaid;
	}
	final String? courierNo = jsonConvert.convert<String>(json['courier_no']);
	if (courierNo != null) {
		orderListData.courierNo = courierNo;
	}
	final String? courierCom = jsonConvert.convert<String>(json['courier_com']);
	if (courierCom != null) {
		orderListData.courierCom = courierCom;
	}
	final int? deliveryWay = jsonConvert.convert<int>(json['delivery_way']);
	if (deliveryWay != null) {
		orderListData.deliveryWay = deliveryWay;
	}
	final int? shipmentTime = jsonConvert.convert<int>(json['shipment_time']);
	if (shipmentTime != null) {
		orderListData.shipmentTime = shipmentTime;
	}
	final int? orderType = jsonConvert.convert<int>(json['order_type']);
	if (orderType != null) {
		orderListData.orderType = orderType;
	}
	final OrderListDataMember? member = jsonConvert.convert<OrderListDataMember>(json['member']);
	if (member != null) {
		orderListData.member = member;
	}
	final List<OrderListDataGoodsList>? goodsList = jsonConvert.convertListNotNull<OrderListDataGoodsList>(json['goods_list']);
	if (goodsList != null) {
		orderListData.goodsList = goodsList;
	}
	return orderListData;
}

Map<String, dynamic> $OrderListDataToJson(OrderListData entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['id'] = entity.id;
	data['order_no'] = entity.orderNo;
	data['time'] = entity.time;
	data['status'] = entity.status;
	data['status_text'] = entity.statusText;
	data['cut_end'] = entity.cutEnd;
	data['after_sale_status'] = entity.afterSaleStatus;
	data['comment_status'] = entity.commentStatus;
	data['type'] = entity.type;
	data['unpaid'] = entity.unpaid;
	data['courier_no'] = entity.courierNo;
	data['courier_com'] = entity.courierCom;
	data['delivery_way'] = entity.deliveryWay;
	data['shipment_time'] = entity.shipmentTime;
	data['order_type'] = entity.orderType;
	data['member'] = entity.member?.toJson();
	data['goods_list'] =  entity.goodsList?.map((v) => v.toJson()).toList();
	return data;
}

OrderListDataMember $OrderListDataMemberFromJson(Map<String, dynamic> json) {
	final OrderListDataMember orderListDataMember = OrderListDataMember();
	final String? nickname = jsonConvert.convert<String>(json['nickname']);
	if (nickname != null) {
		orderListDataMember.nickname = nickname;
	}
	final String? headimgurl = jsonConvert.convert<String>(json['headimgurl']);
	if (headimgurl != null) {
		orderListDataMember.headimgurl = headimgurl;
	}
	final String? statusText = jsonConvert.convert<String>(json['status_text']);
	if (statusText != null) {
		orderListDataMember.statusText = statusText;
	}
	return orderListDataMember;
}

Map<String, dynamic> $OrderListDataMemberToJson(OrderListDataMember entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['nickname'] = entity.nickname;
	data['headimgurl'] = entity.headimgurl;
	data['status_text'] = entity.statusText;
	return data;
}

OrderListDataGoodsList $OrderListDataGoodsListFromJson(Map<String, dynamic> json) {
	final OrderListDataGoodsList orderListDataGoodsList = OrderListDataGoodsList();
	final String? goodsName = jsonConvert.convert<String>(json['goods_name']);
	if (goodsName != null) {
		orderListDataGoodsList.goodsName = goodsName;
	}
	final String? goodsImage = jsonConvert.convert<String>(json['goods_image']);
	if (goodsImage != null) {
		orderListDataGoodsList.goodsImage = goodsImage;
	}
	final int? num = jsonConvert.convert<int>(json['num']);
	if (num != null) {
		orderListDataGoodsList.num = num;
	}
	final String? skuName = jsonConvert.convert<String>(json['sku_name']);
	if (skuName != null) {
		orderListDataGoodsList.skuName = skuName;
	}
	final String? price = jsonConvert.convert<String>(json['price']);
	if (price != null) {
		orderListDataGoodsList.price = price;
	}
	final int? goodsId = jsonConvert.convert<int>(json['goods_id']);
	if (goodsId != null) {
		orderListDataGoodsList.goodsId = goodsId;
	}
	return orderListDataGoodsList;
}

Map<String, dynamic> $OrderListDataGoodsListToJson(OrderListDataGoodsList entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['goods_name'] = entity.goodsName;
	data['goods_image'] = entity.goodsImage;
	data['num'] = entity.num;
	data['sku_name'] = entity.skuName;
	data['price'] = entity.price;
	data['goods_id'] = entity.goodsId;
	return data;
}