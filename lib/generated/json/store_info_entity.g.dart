import 'package:jss_flutter/generated/json/base/json_convert_content.dart';
import 'package:jss_flutter/bean/store_info_entity.dart';

StoreInfoEntity $StoreInfoEntityFromJson(Map<String, dynamic> json) {
	final StoreInfoEntity storeInfoEntity = StoreInfoEntity();
	final int? code = jsonConvert.convert<int>(json['code']);
	if (code != null) {
		storeInfoEntity.code = code;
	}
	final StoreInfoData? data = jsonConvert.convert<StoreInfoData>(json['data']);
	if (data != null) {
		storeInfoEntity.data = data;
	}
	final String? msg = jsonConvert.convert<String>(json['msg']);
	if (msg != null) {
		storeInfoEntity.msg = msg;
	}
	final int? status = jsonConvert.convert<int>(json['status']);
	if (status != null) {
		storeInfoEntity.status = status;
	}
	return storeInfoEntity;
}

Map<String, dynamic> $StoreInfoEntityToJson(StoreInfoEntity entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['code'] = entity.code;
	data['data'] = entity.data?.toJson();
	data['msg'] = entity.msg;
	data['status'] = entity.status;
	return data;
}

StoreInfoData $StoreInfoDataFromJson(Map<String, dynamic> json) {
	final StoreInfoData storeInfoData = StoreInfoData();
	final int? id = jsonConvert.convert<int>(json['id']);
	if (id != null) {
		storeInfoData.id = id;
	}
	final String? name = jsonConvert.convert<String>(json['name']);
	if (name != null) {
		storeInfoData.name = name;
	}
	final int? status = jsonConvert.convert<int>(json['status']);
	if (status != null) {
		storeInfoData.status = status;
	}
	final int? earnestStatus = jsonConvert.convert<int>(json['earnest_status']);
	if (earnestStatus != null) {
		storeInfoData.earnestStatus = earnestStatus;
	}
	final String? logo = jsonConvert.convert<String>(json['logo']);
	if (logo != null) {
		storeInfoData.logo = logo;
	}
	final String? image = jsonConvert.convert<String>(json['image']);
	if (image != null) {
		storeInfoData.image = image;
	}
	final List<String>? images = jsonConvert.convertListNotNull<String>(json['images']);
	if (images != null) {
		storeInfoData.images = images;
	}
	final int? trainStatus = jsonConvert.convert<int>(json['train_status']);
	if (trainStatus != null) {
		storeInfoData.trainStatus = trainStatus;
	}
	final int? trainExpirationTime = jsonConvert.convert<int>(json['train_expiration_time']);
	if (trainExpirationTime != null) {
		storeInfoData.trainExpirationTime = trainExpirationTime;
	}
	final double? lat = jsonConvert.convert<double>(json['lat']);
	if (lat != null) {
		storeInfoData.lat = lat;
	}
	final double? lng = jsonConvert.convert<double>(json['lng']);
	if (lng != null) {
		storeInfoData.lng = lng;
	}
	final String? phone = jsonConvert.convert<String>(json['phone']);
	if (phone != null) {
		storeInfoData.phone = phone;
	}
	final String? businessHours = jsonConvert.convert<String>(json['business_hours']);
	if (businessHours != null) {
		storeInfoData.businessHours = businessHours;
	}
	final int? proId = jsonConvert.convert<int>(json['pro_id']);
	if (proId != null) {
		storeInfoData.proId = proId;
	}
	final int? areaId = jsonConvert.convert<int>(json['area_id']);
	if (areaId != null) {
		storeInfoData.areaId = areaId;
	}
	final int? goodsMode = jsonConvert.convert<int>(json['goods_mode']);
	if (goodsMode != null) {
		storeInfoData.goodsMode = goodsMode;
	}
	final int? cityId = jsonConvert.convert<int>(json['city_id']);
	if (cityId != null) {
		storeInfoData.cityId = cityId;
	}
	final int? streetId = jsonConvert.convert<int>(json['street_id']);
	if (streetId != null) {
		storeInfoData.streetId = streetId;
	}
	final int? tradeId = jsonConvert.convert<int>(json['trade_id']);
	if (tradeId != null) {
		storeInfoData.tradeId = tradeId;
	}
	final String? tradeName = jsonConvert.convert<String>(json['trade_name']);
	if (tradeName != null) {
		storeInfoData.tradeName = tradeName;
	}
	final String? regionInfo = jsonConvert.convert<String>(json['region_info']);
	if (regionInfo != null) {
		storeInfoData.regionInfo = regionInfo;
	}
	final String? address = jsonConvert.convert<String>(json['address']);
	if (address != null) {
		storeInfoData.address = address;
	}
	final String? desc = jsonConvert.convert<String>(json['desc']);
	if (desc != null) {
		storeInfoData.desc = desc;
	}
	final String? auditStatusText = jsonConvert.convert<String>(json['audit_status_text']);
	if (auditStatusText != null) {
		storeInfoData.auditStatusText = auditStatusText;
	}
	final String? earnestStatusText = jsonConvert.convert<String>(json['earnest_status_text']);
	if (earnestStatusText != null) {
		storeInfoData.earnestStatusText = earnestStatusText;
	}
	final String? statusText = jsonConvert.convert<String>(json['status_text']);
	if (statusText != null) {
		storeInfoData.statusText = statusText;
	}
	final String? fansNum = jsonConvert.convert<String>(json['fans_num']);
	if (fansNum != null) {
		storeInfoData.fansNum = fansNum;
	}
	final StoreInfoDataCert? cert = jsonConvert.convert<StoreInfoDataCert>(json['cert']);
	if (cert != null) {
		storeInfoData.cert = cert;
	}
	final String? earnest = jsonConvert.convert<String>(json['earnest']);
	if (earnest != null) {
		storeInfoData.earnest = earnest;
	}
	return storeInfoData;
}

Map<String, dynamic> $StoreInfoDataToJson(StoreInfoData entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['id'] = entity.id;
	data['name'] = entity.name;
	data['status'] = entity.status;
	data['earnest_status'] = entity.earnestStatus;
	data['logo'] = entity.logo;
	data['image'] = entity.image;
	data['images'] =  entity.images;
	data['train_status'] = entity.trainStatus;
	data['train_expiration_time'] = entity.trainExpirationTime;
	data['lat'] = entity.lat;
	data['lng'] = entity.lng;
	data['phone'] = entity.phone;
	data['business_hours'] = entity.businessHours;
	data['pro_id'] = entity.proId;
	data['area_id'] = entity.areaId;
	data['goods_mode'] = entity.goodsMode;
	data['city_id'] = entity.cityId;
	data['street_id'] = entity.streetId;
	data['trade_id'] = entity.tradeId;
	data['trade_name'] = entity.tradeName;
	data['region_info'] = entity.regionInfo;
	data['address'] = entity.address;
	data['desc'] = entity.desc;
	data['audit_status_text'] = entity.auditStatusText;
	data['earnest_status_text'] = entity.earnestStatusText;
	data['status_text'] = entity.statusText;
	data['fans_num'] = entity.fansNum;
	data['cert'] = entity.cert?.toJson();
	data['earnest'] = entity.earnest;
	return data;
}

StoreInfoDataCert $StoreInfoDataCertFromJson(Map<String, dynamic> json) {
	final StoreInfoDataCert storeInfoDataCert = StoreInfoDataCert();
	final int? id = jsonConvert.convert<int>(json['id']);
	if (id != null) {
		storeInfoDataCert.id = id;
	}
	final String? comName = jsonConvert.convert<String>(json['com_name']);
	if (comName != null) {
		storeInfoDataCert.comName = comName;
	}
	final String? nature = jsonConvert.convert<String>(json['nature']);
	if (nature != null) {
		storeInfoDataCert.nature = nature;
	}
	final String? comShengshiqu = jsonConvert.convert<String>(json['com_shengshiqu']);
	if (comShengshiqu != null) {
		storeInfoDataCert.comShengshiqu = comShengshiqu;
	}
	final String? comAddress = jsonConvert.convert<String>(json['com_address']);
	if (comAddress != null) {
		storeInfoDataCert.comAddress = comAddress;
	}
	final String? fixedPhone = jsonConvert.convert<String>(json['fixed_phone']);
	if (fixedPhone != null) {
		storeInfoDataCert.fixedPhone = fixedPhone;
	}
	final String? comEmail = jsonConvert.convert<String>(json['com_email']);
	if (comEmail != null) {
		storeInfoDataCert.comEmail = comEmail;
	}
	final String? zczj = jsonConvert.convert<String>(json['zczj']);
	if (zczj != null) {
		storeInfoDataCert.zczj = zczj;
	}
	final String? tyxydm = jsonConvert.convert<String>(json['tyxydm']);
	if (tyxydm != null) {
		storeInfoDataCert.tyxydm = tyxydm;
	}
	final String? farenName = jsonConvert.convert<String>(json['faren_name']);
	if (farenName != null) {
		storeInfoDataCert.farenName = farenName;
	}
	final int? zzstartTime = jsonConvert.convert<int>(json['zzstart_time']);
	if (zzstartTime != null) {
		storeInfoDataCert.zzstartTime = zzstartTime;
	}
	final int? zzendTime = jsonConvert.convert<int>(json['zzend_time']);
	if (zzendTime != null) {
		storeInfoDataCert.zzendTime = zzendTime;
	}
	final String? jyfw = jsonConvert.convert<String>(json['jyfw']);
	if (jyfw != null) {
		storeInfoDataCert.jyfw = jyfw;
	}
	final String? jiancheng = jsonConvert.convert<String>(json['jiancheng']);
	if (jiancheng != null) {
		storeInfoDataCert.jiancheng = jiancheng;
	}
	final String? sfzNum = jsonConvert.convert<String>(json['sfz_num']);
	if (sfzNum != null) {
		storeInfoDataCert.sfzNum = sfzNum;
	}
	final String? sfzzPic = jsonConvert.convert<String>(json['sfzz_pic']);
	if (sfzzPic != null) {
		storeInfoDataCert.sfzzPic = sfzzPic;
	}
	final String? sfzbPic = jsonConvert.convert<String>(json['sfzb_pic']);
	if (sfzbPic != null) {
		storeInfoDataCert.sfzbPic = sfzbPic;
	}
	final String? frsfzPic = jsonConvert.convert<String>(json['frsfz_pic']);
	if (frsfzPic != null) {
		storeInfoDataCert.frsfzPic = frsfzPic;
	}
	final String? zhizhao = jsonConvert.convert<String>(json['zhizhao']);
	if (zhizhao != null) {
		storeInfoDataCert.zhizhao = zhizhao;
	}
	final int? applyType = jsonConvert.convert<int>(json['apply_type']);
	if (applyType != null) {
		storeInfoDataCert.applyType = applyType;
	}
	final int? applyTime = jsonConvert.convert<int>(json['apply_time']);
	if (applyTime != null) {
		storeInfoDataCert.applyTime = applyTime;
	}
	final int? shopId = jsonConvert.convert<int>(json['shop_id']);
	if (shopId != null) {
		storeInfoDataCert.shopId = shopId;
	}
	final int? sfzLimit = jsonConvert.convert<int>(json['sfz_limit']);
	if (sfzLimit != null) {
		storeInfoDataCert.sfzLimit = sfzLimit;
	}
	final String? sfzTime = jsonConvert.convert<String>(json['sfz_time']);
	if (sfzTime != null) {
		storeInfoDataCert.sfzTime = sfzTime;
	}
	final int? isSzhy = jsonConvert.convert<int>(json['is_szhy']);
	if (isSzhy != null) {
		storeInfoDataCert.isSzhy = isSzhy;
	}
	final String? khxkz = jsonConvert.convert<String>(json['khxkz']);
	if (khxkz != null) {
		storeInfoDataCert.khxkz = khxkz;
	}
	final String? zhizhaoNum = jsonConvert.convert<String>(json['zhizhao_num']);
	if (zhizhaoNum != null) {
		storeInfoDataCert.zhizhaoNum = zhizhaoNum;
	}
	final String? zzjgdm = jsonConvert.convert<String>(json['zzjgdm']);
	if (zzjgdm != null) {
		storeInfoDataCert.zzjgdm = zzjgdm;
	}
	final String? nsrsbm = jsonConvert.convert<String>(json['nsrsbm']);
	if (nsrsbm != null) {
		storeInfoDataCert.nsrsbm = nsrsbm;
	}
	final String? zzjgdmPic = jsonConvert.convert<String>(json['zzjgdm_pic']);
	if (zzjgdmPic != null) {
		storeInfoDataCert.zzjgdmPic = zzjgdmPic;
	}
	final String? nsrsbmPic = jsonConvert.convert<String>(json['nsrsbm_pic']);
	if (nsrsbmPic != null) {
		storeInfoDataCert.nsrsbmPic = nsrsbmPic;
	}
	final String? zzjgdmTime = jsonConvert.convert<String>(json['zzjgdm_time']);
	if (zzjgdmTime != null) {
		storeInfoDataCert.zzjgdmTime = zzjgdmTime;
	}
	final String? createdAt = jsonConvert.convert<String>(json['created_at']);
	if (createdAt != null) {
		storeInfoDataCert.createdAt = createdAt;
	}
	final String? updatedAt = jsonConvert.convert<String>(json['updated_at']);
	if (updatedAt != null) {
		storeInfoDataCert.updatedAt = updatedAt;
	}
	final dynamic? deletedAt = jsonConvert.convert<dynamic>(json['deleted_at']);
	if (deletedAt != null) {
		storeInfoDataCert.deletedAt = deletedAt;
	}
	final String? typeText = jsonConvert.convert<String>(json['type_text']);
	if (typeText != null) {
		storeInfoDataCert.typeText = typeText;
	}
	final String? sfzLimitText = jsonConvert.convert<String>(json['sfz_limit_text']);
	if (sfzLimitText != null) {
		storeInfoDataCert.sfzLimitText = sfzLimitText;
	}
	final String? isSzhyText = jsonConvert.convert<String>(json['is_szhy_text']);
	if (isSzhyText != null) {
		storeInfoDataCert.isSzhyText = isSzhyText;
	}
	return storeInfoDataCert;
}

Map<String, dynamic> $StoreInfoDataCertToJson(StoreInfoDataCert entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['id'] = entity.id;
	data['com_name'] = entity.comName;
	data['nature'] = entity.nature;
	data['com_shengshiqu'] = entity.comShengshiqu;
	data['com_address'] = entity.comAddress;
	data['fixed_phone'] = entity.fixedPhone;
	data['com_email'] = entity.comEmail;
	data['zczj'] = entity.zczj;
	data['tyxydm'] = entity.tyxydm;
	data['faren_name'] = entity.farenName;
	data['zzstart_time'] = entity.zzstartTime;
	data['zzend_time'] = entity.zzendTime;
	data['jyfw'] = entity.jyfw;
	data['jiancheng'] = entity.jiancheng;
	data['sfz_num'] = entity.sfzNum;
	data['sfzz_pic'] = entity.sfzzPic;
	data['sfzb_pic'] = entity.sfzbPic;
	data['frsfz_pic'] = entity.frsfzPic;
	data['zhizhao'] = entity.zhizhao;
	data['apply_type'] = entity.applyType;
	data['apply_time'] = entity.applyTime;
	data['shop_id'] = entity.shopId;
	data['sfz_limit'] = entity.sfzLimit;
	data['sfz_time'] = entity.sfzTime;
	data['is_szhy'] = entity.isSzhy;
	data['khxkz'] = entity.khxkz;
	data['zhizhao_num'] = entity.zhizhaoNum;
	data['zzjgdm'] = entity.zzjgdm;
	data['nsrsbm'] = entity.nsrsbm;
	data['zzjgdm_pic'] = entity.zzjgdmPic;
	data['nsrsbm_pic'] = entity.nsrsbmPic;
	data['zzjgdm_time'] = entity.zzjgdmTime;
	data['created_at'] = entity.createdAt;
	data['updated_at'] = entity.updatedAt;
	data['deleted_at'] = entity.deletedAt;
	data['type_text'] = entity.typeText;
	data['sfz_limit_text'] = entity.sfzLimitText;
	data['is_szhy_text'] = entity.isSzhyText;
	return data;
}