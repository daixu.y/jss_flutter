import 'package:jss_flutter/generated/json/base/json_convert_content.dart';
import 'package:jss_flutter/bean/withdraw_record_entity.dart';

WithdrawRecordEntity $WithdrawRecordEntityFromJson(Map<String, dynamic> json) {
	final WithdrawRecordEntity withdrawRecordEntity = WithdrawRecordEntity();
	final int? code = jsonConvert.convert<int>(json['code']);
	if (code != null) {
		withdrawRecordEntity.code = code;
	}
	final List<WithdrawRecordData>? data = jsonConvert.convertListNotNull<WithdrawRecordData>(json['data']);
	if (data != null) {
		withdrawRecordEntity.data = data;
	}
	final String? msg = jsonConvert.convert<String>(json['msg']);
	if (msg != null) {
		withdrawRecordEntity.msg = msg;
	}
	final int? status = jsonConvert.convert<int>(json['status']);
	if (status != null) {
		withdrawRecordEntity.status = status;
	}
	return withdrawRecordEntity;
}

Map<String, dynamic> $WithdrawRecordEntityToJson(WithdrawRecordEntity entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['code'] = entity.code;
	data['data'] =  entity.data?.map((v) => v.toJson()).toList();
	data['msg'] = entity.msg;
	data['status'] = entity.status;
	return data;
}

WithdrawRecordData $WithdrawRecordDataFromJson(Map<String, dynamic> json) {
	final WithdrawRecordData withdrawRecordData = WithdrawRecordData();
	final String? money = jsonConvert.convert<String>(json['money']);
	if (money != null) {
		withdrawRecordData.money = money;
	}
	final String? accountMoney = jsonConvert.convert<String>(json['account_money']);
	if (accountMoney != null) {
		withdrawRecordData.accountMoney = accountMoney;
	}
	final String? serviceCharge = jsonConvert.convert<String>(json['service_charge']);
	if (serviceCharge != null) {
		withdrawRecordData.serviceCharge = serviceCharge;
	}
	final String? statusText = jsonConvert.convert<String>(json['status_text']);
	if (statusText != null) {
		withdrawRecordData.statusText = statusText;
	}
	final int? status = jsonConvert.convert<int>(json['status']);
	if (status != null) {
		withdrawRecordData.status = status;
	}
	final String? time = jsonConvert.convert<String>(json['time']);
	if (time != null) {
		withdrawRecordData.time = time;
	}
	final String? auditTime = jsonConvert.convert<String>(json['audit_time']);
	if (auditTime != null) {
		withdrawRecordData.auditTime = auditTime;
	}
	final int? id = jsonConvert.convert<int>(json['id']);
	if (id != null) {
		withdrawRecordData.id = id;
	}
	final String? remark = jsonConvert.convert<String>(json['remark']);
	if (remark != null) {
		withdrawRecordData.remark = remark;
	}
	final String? bankName = jsonConvert.convert<String>(json['bank_name']);
	if (bankName != null) {
		withdrawRecordData.bankName = bankName;
	}
	final String? cardNo = jsonConvert.convert<String>(json['card_no']);
	if (cardNo != null) {
		withdrawRecordData.cardNo = cardNo;
	}
	return withdrawRecordData;
}

Map<String, dynamic> $WithdrawRecordDataToJson(WithdrawRecordData entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['money'] = entity.money;
	data['account_money'] = entity.accountMoney;
	data['service_charge'] = entity.serviceCharge;
	data['status_text'] = entity.statusText;
	data['status'] = entity.status;
	data['time'] = entity.time;
	data['audit_time'] = entity.auditTime;
	data['id'] = entity.id;
	data['remark'] = entity.remark;
	data['bank_name'] = entity.bankName;
	data['card_no'] = entity.cardNo;
	return data;
}