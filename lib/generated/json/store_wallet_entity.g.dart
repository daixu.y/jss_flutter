import 'package:jss_flutter/generated/json/base/json_convert_content.dart';
import 'package:jss_flutter/bean/store_wallet_entity.dart';

StoreWalletEntity $StoreWalletEntityFromJson(Map<String, dynamic> json) {
	final StoreWalletEntity storeWalletEntity = StoreWalletEntity();
	final int? code = jsonConvert.convert<int>(json['code']);
	if (code != null) {
		storeWalletEntity.code = code;
	}
	final StoreWalletData? data = jsonConvert.convert<StoreWalletData>(json['data']);
	if (data != null) {
		storeWalletEntity.data = data;
	}
	final String? msg = jsonConvert.convert<String>(json['msg']);
	if (msg != null) {
		storeWalletEntity.msg = msg;
	}
	final int? status = jsonConvert.convert<int>(json['status']);
	if (status != null) {
		storeWalletEntity.status = status;
	}
	return storeWalletEntity;
}

Map<String, dynamic> $StoreWalletEntityToJson(StoreWalletEntity entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['code'] = entity.code;
	data['data'] = entity.data?.toJson();
	data['msg'] = entity.msg;
	data['status'] = entity.status;
	return data;
}

StoreWalletData $StoreWalletDataFromJson(Map<String, dynamic> json) {
	final StoreWalletData storeWalletData = StoreWalletData();
	final String? totalMoney = jsonConvert.convert<String>(json['total_money']);
	if (totalMoney != null) {
		storeWalletData.totalMoney = totalMoney;
	}
	final String? shopMoney = jsonConvert.convert<String>(json['shop_money']);
	if (shopMoney != null) {
		storeWalletData.shopMoney = shopMoney;
	}
	final String? shopAccountFrozen = jsonConvert.convert<String>(json['shop_account_frozen']);
	if (shopAccountFrozen != null) {
		storeWalletData.shopAccountFrozen = shopAccountFrozen;
	}
	final String? shopNoSettlement = jsonConvert.convert<String>(json['shop_no_settlement']);
	if (shopNoSettlement != null) {
		storeWalletData.shopNoSettlement = shopNoSettlement;
	}
	final String? shopMoneyWithdrawTotal = jsonConvert.convert<String>(json['shop_money_withdraw_total']);
	if (shopMoneyWithdrawTotal != null) {
		storeWalletData.shopMoneyWithdrawTotal = shopMoneyWithdrawTotal;
	}
	final String? barterMoney = jsonConvert.convert<String>(json['barter_money']);
	if (barterMoney != null) {
		storeWalletData.barterMoney = barterMoney;
	}
	final String? yesterdayMoney = jsonConvert.convert<String>(json['yesterday_money']);
	if (yesterdayMoney != null) {
		storeWalletData.yesterdayMoney = yesterdayMoney;
	}
	return storeWalletData;
}

Map<String, dynamic> $StoreWalletDataToJson(StoreWalletData entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['total_money'] = entity.totalMoney;
	data['shop_money'] = entity.shopMoney;
	data['shop_account_frozen'] = entity.shopAccountFrozen;
	data['shop_no_settlement'] = entity.shopNoSettlement;
	data['shop_money_withdraw_total'] = entity.shopMoneyWithdrawTotal;
	data['barter_money'] = entity.barterMoney;
	data['yesterday_money'] = entity.yesterdayMoney;
	return data;
}