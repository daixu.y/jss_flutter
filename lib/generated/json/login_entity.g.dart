import 'package:jss_flutter/generated/json/base/json_convert_content.dart';
import 'package:jss_flutter/bean/login_entity.dart';

LoginEntity $LoginEntityFromJson(Map<String, dynamic> json) {
	final LoginEntity loginEntity = LoginEntity();
	final int? code = jsonConvert.convert<int>(json['code']);
	if (code != null) {
		loginEntity.code = code;
	}
	final LoginData? data = jsonConvert.convert<LoginData>(json['data']);
	if (data != null) {
		loginEntity.data = data;
	}
	final String? msg = jsonConvert.convert<String>(json['msg']);
	if (msg != null) {
		loginEntity.msg = msg;
	}
	final int? status = jsonConvert.convert<int>(json['status']);
	if (status != null) {
		loginEntity.status = status;
	}
	return loginEntity;
}

Map<String, dynamic> $LoginEntityToJson(LoginEntity entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['code'] = entity.code;
	data['data'] = entity.data?.toJson();
	data['msg'] = entity.msg;
	data['status'] = entity.status;
	return data;
}

LoginData $LoginDataFromJson(Map<String, dynamic> json) {
	final LoginData loginData = LoginData();
	final String? token = jsonConvert.convert<String>(json['token']);
	if (token != null) {
		loginData.token = token;
	}
	return loginData;
}

Map<String, dynamic> $LoginDataToJson(LoginData entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['token'] = entity.token;
	return data;
}