import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'logic.dart';

class SettingPage extends StatelessWidget {
  final logic = Get.find<SettingLogic>();
  final state = Get.find<SettingLogic>().state;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          const Padding(padding: EdgeInsets.all(20)),
          ElevatedButton(
            onPressed: () => {
              Get.dialog(
                _createSimpleDialog("", "确定退出登录?"),
              ),
            },
            child: const Text("退出登录"),
          )
        ],
      ),
    );
  }

  Widget _dialogButton(String data, VoidCallback onPressed,
      {double height = 50,
      double fontSize = 16,
      int foregroundColor = 0xff999999}) {
    return SizedBox(
      height: height,
      child: TextButton(
        onPressed: onPressed,
        style: ButtonStyle(
          textStyle: MaterialStateProperty.all(
            TextStyle(fontSize: fontSize),
          ),
          foregroundColor: MaterialStateProperty.all(
            Color(foregroundColor),
          ),
        ),
        child: Text(data),
      ),
    );
  }

  Widget _createSimpleDialog(String titleText, String contentText,
      {String confirmText = "确定",
      String cancelText = "取消",
      int confirmColor = 0xff999999,
      int cancelColor = 0xffff3300}) {
    bool isShow;
    if (titleText.isEmpty) {
      isShow = false;
    } else {
      isShow = true;
    }
    return AlertDialog(
      contentPadding: const EdgeInsets.all(20),
      title: isShow ? Text(titleText, textAlign: TextAlign.center) : null,
      titleTextStyle: const TextStyle(
        fontSize: 20,
        fontWeight: FontWeight.w500,
        color: Color(0xffff0000),
      ),
      content: Text(
        contentText,
        textAlign: TextAlign.center,
      ),
      contentTextStyle: const TextStyle(
        fontSize: 16,
        fontWeight: FontWeight.w400,
        color: Color(0xff333333),
      ),
      actions: <Widget>[
        Column(
          children: [
            const Divider(height: 0.5, color: Color(0xffe9e9e9)),
            Flex(
              direction: Axis.horizontal,
              children: <Widget>[
                Expanded(
                  child: _dialogButton(
                    cancelText,
                    () {
                      Get.back();
                    },
                    foregroundColor: cancelColor,
                  ),
                ),
                const VerticalDivider(
                  width: 0.5,
                  color: Color(0xffe9e9e9),
                ),
                Expanded(
                  child: _dialogButton(
                    confirmText,
                    () {
                      Get.back();
                    },
                    foregroundColor: confirmColor,
                  ),
                ),
              ],
            )
          ],
        ),
      ],
      buttonPadding: const EdgeInsets.all(0),
    );
  }
}
