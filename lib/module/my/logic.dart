import 'package:get/get.dart';
import 'package:jss_flutter/app/config/route_config.dart';

import 'state.dart';

class MyLogic extends GetxController {
  final MyState state = MyState();

  void jumpSetting() => Get.toNamed(RouteConfig.setting);
}
