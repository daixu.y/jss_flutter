import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'logic.dart';

class MyPage extends GetView<MyLogic> {
  const MyPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          Stack(children: [
            Image.asset(
              "assets/images/bg_my_top.webp",
              fit: BoxFit.cover,
              width: double.infinity,
              height: 280,
            ),
            Column(
              children: [
                _userBasicInfoWidget(),
                _userAccountPandectWidget(),
              ],
            ),
          ]),
        ],
      ),
    );
  }

  Widget _userBasicInfoWidget() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(20, 65, 20, 0),
      child: Row(
        children: [
          _userAvatarWidget(),
          const Padding(padding: EdgeInsets.only(left: 15)),
          _loginOptionWidget(),
          const Spacer(),
          _settingOptionWidget(),
        ],
      ),
    );
  }

  Widget _settingOptionWidget() {
    return InkWell(
      onTap: () => controller.jumpSetting(),
      child: const Icon(
        Icons.settings,
        size: 20,
        color: Colors.white,
      ),
    );
  }

  Widget _loginOptionWidget() {
    return ElevatedButton(
      onPressed: () => {},
      style: ButtonStyle(
        minimumSize: MaterialStateProperty.all(
          const Size(102, 32),
        ),
        shape: MaterialStateProperty.all(
          const RoundedRectangleBorder(
            borderRadius: BorderRadius.all(
              Radius.circular(18),
            ),
          ),
        ),
        backgroundColor: MaterialStateProperty.all(
          Colors.white,
        ),
      ),
      child: const Text(
        "登录/注册",
        style: TextStyle(
          fontSize: 16,
          color: Color(0xffff3300),
        ),
      ),
    );
  }

  Widget _userAvatarWidget() {
    return ClipOval(
      child: CachedNetworkImage(
        placeholder: (context, url) => Image.asset('assets/images/logo.png'),
        errorWidget: (context, url, error) =>
            Image.asset('assets/images/logo.png'),
        imageUrl:
            "https://i3.hoopchina.com.cn/hupuapp/bbs/13/42314013/thread_42314013_20201026230808_s_3123_o_w_152_h_160_70752.jpg?x-oss-process=image/resize,w_225/qulity,Q_60",
        width: 56,
        height: 56,
        fit: BoxFit.cover,
      ),
    );
  }

  Widget _userAccountItemWidget(String title, String value) {
    return Expanded(
      child: Column(
        children: [
          Text(
            title,
            style: const TextStyle(
              fontSize: 14,
              color: Color(0xff666666),
            ),
          ),
          const Padding(padding: EdgeInsets.only(top: 10)),
          Text(
            value,
            style: const TextStyle(
              fontSize: 14,
              color: Color(0xff333333),
            ),
          ),
        ],
      ),
    );
  }

  Widget _userAccountPandectWidget() {
    return Container(
      height: 140,
      margin: const EdgeInsets.fromLTRB(10, 20, 10, 10),
      padding: const EdgeInsets.all(15),
      decoration: const BoxDecoration(
        //背景
        color: Colors.white,
        //设置四周圆角 角度
        borderRadius: BorderRadius.all(Radius.circular(10)),
      ),
      child: Column(
        children: [
          Row(
            children: const [
              Text(
                "账户总览(元)",
                style: TextStyle(
                  fontSize: 16,
                  color: Color(0xff333333),
                ),
              ),
              Spacer(),
              Text(
                "0.00",
                style: TextStyle(
                  fontSize: 18,
                  color: Color(0xff333333),
                ),
              )
            ],
          ),
          const Padding(
            padding: EdgeInsets.fromLTRB(0, 15, 0, 20),
            child: Divider(height: 0.5, color: Color(0xffe9e9e9)),
          ),
          Row(
            children: [
              _userAccountItemWidget("砍价券", "0"),
              _userAccountItemWidget("卡券", "0"),
              _userAccountItemWidget("易货券", "0"),
            ],
          ),
        ],
      ),
    );
  }
}
