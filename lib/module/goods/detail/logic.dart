import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:jss_flutter/bean/goods_detail_entity.dart';
import 'package:jss_flutter/http/net/http_client.dart';
import 'package:jss_flutter/http/net/http_response.dart';
import 'package:jss_flutter/utils/log_utils.dart';

import 'state.dart';

class DetailLogic extends GetxController {
  final DetailState state = DetailState();
  var storageBox = GetStorage();
  final HttpClient dio = Get.find<HttpClient>();

  @override
  void onInit() {
    super.onInit();
    int id = Get.arguments;
    getBarterGoodsList(id);
  }

  /// 0 未加载 1 加载成功 2 加载失败
  void getBarterGoodsList(int id) async {
    var data = {"lat": "28.188452", "lng": "113.010063", "id": id};

    HttpResponse appResponse = await dio.post("goods/get_info", data: data);
    if (appResponse.ok) {
      var response = GoodsDetailEntity.fromJson(appResponse.data);
      if (null != response.data) {
        state.data.value = response.data!;
        state.loadState.value = 1;
        response.data?.images?.forEach((item) {
          if (item.type == 1) {
            state.bannerImages.add(item);
          } else if (item.type == 2) {
            state.detailImages.add(item);
          }
        });
      } else {
        state.loadState.value = 2;
      }
    } else {
      LogUtils.e("==fail==${appResponse.error}");
      state.loadState.value = 2;
    }
  }
}
