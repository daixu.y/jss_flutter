import 'package:get/get.dart';
import 'package:jss_flutter/bean/goods_detail_entity.dart';

class DetailState {
  final data = GoodsDetailData().obs;
  final bannerImages = <GoodsDetailDataImages>[].obs;
  final detailImages = <GoodsDetailDataImages>[].obs;

  /// 0 未加载 1 加载成功 2 加载失败
  final loadState = 0.obs;

  DetailState() {
    ///Initialize variables
  }
}
