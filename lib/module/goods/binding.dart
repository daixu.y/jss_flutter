import 'package:get/get.dart';

import 'logic.dart';

class GoodsBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => GoodsLogic());
  }
}
