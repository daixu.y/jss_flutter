import 'package:get/get.dart';
import 'package:jss_flutter/utils/log_utils.dart';

import 'state.dart';

class PromotionsLogic extends GetxController {
  final PromotionsState state = PromotionsState();

  @override
  void onInit() {
    super.onInit();
    LogUtils.e("PromotionsLogic onInit");
  }

}
