import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'logic.dart';

class PromotionsPage extends GetView<PromotionsLogic> {
  const PromotionsPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // Get.put(PromotionsLogic);
    // final logic = Get.put(PromotionsLogic);
    // final state = Get.find<PromotionsLogic>().state;
    return Center(child: Text("优惠${controller.state.msg}"));
  }
}
