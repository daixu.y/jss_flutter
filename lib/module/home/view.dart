import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:jss_flutter/bean/barter_goods_entity.dart';
import 'package:jss_flutter/bean/goods_response_entity.dart';
import 'package:jss_flutter/module/home/widgets/home_app_bar.dart';

import 'logic.dart';

///首页
class HomePage extends GetView<HomeLogic> {
  const HomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const HomeAppBar(),
      backgroundColor: const Color(0xfff5f5f5),
      body: ListView(
        children: [
          _topItem(),
          Obx(
            () => Offstage(
              offstage: controller.state.offstage.value,
              child: _activityWidget(),
            ),
          ),
          _goodsListWidget()
        ],
      ),
    );
  }

  Widget _topItem() {
    return Container(
      margin: const EdgeInsets.only(top: 10),
      height: 90,
      child: Card(
        color: Colors.white,
        elevation: 0,
        shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.all(
            Radius.circular(10),
          ),
        ),
        margin: const EdgeInsets.fromLTRB(10, 0, 10, 0),
        child: SizedBox(
          child: GridView.count(
            padding: const EdgeInsets.fromLTRB(10, 15, 10, 10),
            // crossAxisSpacing: 10.0,
            //纵轴间距
            mainAxisSpacing: 10,
            //横轴元素个数
            crossAxisCount: 5,
            children: <Widget>[
              _topGridItem(
                "一元区",
                () => controller.jumpStore(),
              ),
              _topGridItem(
                "十元区",
                () => {},
              ),
              _topGridItem(
                "品牌区",
                () => {},
              ),
              _topGridItem(
                "智库区",
                () => {},
              ),
              _topGridItem(
                "易货区",
                () => {},
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _topGridItem(String title, GestureTapCallback? onTap) {
    return InkWell(
      onTap: onTap,
      child: SizedBox(
        height: 90,
        width: 50,
        child: Column(
          children: [
            Image.asset(
              "assets/images/logo.png",
              width: 44,
              height: 44,
            ),
            Text(title)
          ],
        ),
      ),
    );
  }

  Widget _goodsListWidget() {
    return Obx(
      () => GridView.builder(
        padding: const EdgeInsets.only(left: 8, right: 8),
        shrinkWrap: true,
        physics: const NeverScrollableScrollPhysics(),
        itemCount: controller.state.goodsList.length,
        // scrollDirection: Axis.vertical,
        itemBuilder: (context, index) {
          return _goodsItem(controller.state.goodsList[index], index);
        },
        gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
          //横轴元素个数
          crossAxisCount: 2,
          //纵轴间距
          mainAxisSpacing: 5,
          //横轴间距
          crossAxisSpacing: 4,
          //子组件宽高长度比例
          childAspectRatio: 0.75,
        ),
      ),
    );
  }

  Widget _goodsCoverWidget(GoodsResponseData bean) {
    return ClipRRect(
      borderRadius: const BorderRadius.only(
        topLeft: Radius.circular(6),
        topRight: Radius.circular(6),
      ),
      child: CachedNetworkImage(
        memCacheHeight: 200,
        placeholder: (context, url) =>
            Image.asset('assets/images/default_goods_cover.webp'),
        errorWidget: (context, url, error) =>
            Image.asset('assets/images/default_goods_cover.webp'),
        imageUrl: bean.image,
        width: double.infinity,
        height: 173,
        fit: BoxFit.cover,
      ),
    );
  }

  Widget _goodsInfoWidget(GoodsResponseData bean) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(8, 10, 8, 10),
      child: Text.rich(
        TextSpan(
          children: [
            WidgetSpan(
              alignment: PlaceholderAlignment.middle,
              child: Image.asset(
                "assets/images/logo.png",
                width: 24,
                height: 16,
              ),
            ),
            TextSpan(
              text: bean.name,
              style: const TextStyle(
                fontSize: 12,
                color: Color(0xff333333),
                overflow: TextOverflow.ellipsis,
              ),
            ),
          ],
        ),
        maxLines: 2,
      ),
    );
  }

  Widget _goodsItem(GoodsResponseData bean, int index) {
    return Card(
      color: Colors.white,
      elevation: 0,
      shape: const RoundedRectangleBorder(
        borderRadius: BorderRadius.all(
          Radius.circular(6),
        ),
      ),
      // margin: const EdgeInsets.fromLTRB(15, 0, 15, 0),
      child: InkWell(
        borderRadius: const BorderRadius.all(Radius.circular(6)),
        onTap: () => controller.jumpDetail(bean.id),
        child: SizedBox(
          width: 273,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              _goodsCoverWidget(bean),
              _goodsInfoWidget(bean),
            ],
          ),
        ),
      ),
    );
  }

  Widget _activityWidget() {
    return Container(
      margin: const EdgeInsets.all(10),
      decoration: const BoxDecoration(
        gradient: LinearGradient(
          begin: Alignment.topLeft,
          end: Alignment.bottomRight,
          colors: [
            Color(0xffFFBEBE),
            Color(0xffFFE8E8),
          ],
        ),
        borderRadius: BorderRadius.all(
          Radius.circular(12),
        ),
      ),
      child: Column(
        children: [
          _activityTitleItem(),
          _activityItem(),
        ],
      ),
    );
  }

  Widget _activityItem() {
    return Container(
      decoration: BoxDecoration(
        color: const Color(0xFFFFFFFF),
        borderRadius: BorderRadius.circular(12),
      ),
      padding: const EdgeInsets.only(top: 10, left: 10, right: 10),
      margin: const EdgeInsets.only(left: 10, right: 10, bottom: 10),
      child: SizedBox(
        height: 100,
        child: Obx(
          () => ListView.separated(
            itemCount: controller.state.items.length,
            scrollDirection: Axis.horizontal,
            itemBuilder: (context, index) {
              return _buildItem(controller.state.items[index], index);
            },
            separatorBuilder: (BuildContext context, int index) {
              return const VerticalDivider(
                width: 10,
                color: Color(0xFFFFFFFF),
              );
            },
          ),
        ),
      ),
    );
  }

  Widget _activityTitleItem() {
    return Row(
      children: [
        const Padding(padding: EdgeInsets.only(left: 10)),
        const Text(
          "限时秒杀",
          style: TextStyle(
            fontSize: 22,
            color: Color(0xffDE0000),
            fontWeight: FontWeight.w500,
          ),
        ),
        const Spacer(),
        TextButton(
          onPressed: () {
            controller.jumpStore();
          },
          child: const Text(
            "更多>>",
            style: TextStyle(
              fontSize: 14,
              color: Color(0xffDE0000),
            ),
          ),
        ),
      ],
    );
  }

  Widget _buildItem(BarterGoodsData item, int index) {
    return SizedBox(
      width: 75,
      child: Column(
        children: [
          _activityGoodsCoverWidget(item),
          _activityGoodsNameWidget(item),
        ],
      ),
    );
  }

  Widget _activityGoodsCoverWidget(BarterGoodsData item) {
    return ClipRRect(
      borderRadius: BorderRadius.circular(6),
      child: CachedNetworkImage(
        placeholder: (context, url) =>
            Image.asset('assets/images/default_goods_cover.webp'),
        errorWidget: (context, url, error) =>
            Image.asset('assets/images/default_goods_cover.webp'),
        imageUrl: item.goodsImage,
        width: 75,
        height: 75,
        fit: BoxFit.cover,
      ),
    );
  }

  Widget _activityGoodsNameWidget(BarterGoodsData item) {
    return Text(
      "${item.goodsName}",
      style: const TextStyle(
        fontSize: 14,
        color: Color(0xff666666),
      ),
      maxLines: 1,
      overflow: TextOverflow.ellipsis,
    );
  }
}
