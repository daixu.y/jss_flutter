import 'package:get/get.dart';
import 'package:jss_flutter/bean/barter_goods_entity.dart';
import 'package:jss_flutter/bean/goods_response_entity.dart';

class HomeState {
  var msg;
  final items = <BarterGoodsData>[].obs;
  final goodsList = <GoodsResponseData>[].obs;
  final offstage = true.obs;
  final page = 1.obs;

  HomeState() {
    msg = "home首页";
  }
}
