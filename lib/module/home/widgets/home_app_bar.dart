import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class HomeAppBar extends StatefulWidget implements PreferredSizeWidget {
  const HomeAppBar({Key? key, this.height = 100}) : super(key: key);

  final double height;

  @override
  State<HomeAppBar> createState() => _HomeAppBarState();

  @override
  Size get preferredSize => Size.fromHeight(height);
}

class _HomeAppBarState extends State<HomeAppBar> {
  @override
  Widget build(BuildContext context) {
    return AnnotatedRegion<SystemUiOverlayStyle>(
      value: SystemUiOverlayStyle.dark,
      child: Container(
        padding: const EdgeInsets.only(top: 40, bottom: 15),
        color: const Color(0xffffffff),
        child: _searchField("搜索"),
      ),
    );
  }

  Widget _searchField(String hint) {
    return Padding(
      padding: const EdgeInsets.only(left: 15, right: 15),
      child: Container(
        height: 34,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(17),
          border: Border.all(
            width: 1.5,
            color: const Color(0xffff3300),
          ),
          color: const Color(0xffffffff),
        ),
        child: Row(
          children: [
            Expanded(
              child: TextField(
                maxLines: 1,
                textAlign: TextAlign.start,
                decoration: InputDecoration(
                  hintText: hint,
                  contentPadding: const EdgeInsets.fromLTRB(0, 10, 20, 10),
                  border: InputBorder.none,
                  icon: const Padding(
                    padding: EdgeInsets.only(left: 10),
                    child: Icon(
                      Icons.search,
                      color: Color(0xffff3300),
                    ),
                  ),
                ),
              ),
            ),
            Container(
              margin: const EdgeInsets.only(right: 1, top: 1, bottom: 1),
              width: 50,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(17),
                color: const Color(0xffff3300),
              ),
              child: TextButton(
                onPressed: () {},
                style: ButtonStyle(
                  padding: MaterialStateProperty.all(EdgeInsets.zero),
                ),
                child: const Text(
                  "搜索",
                  style: TextStyle(color: Color(0xffffffff), fontSize: 14),
                  textAlign: TextAlign.center,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
