import 'package:get/get.dart';

import '../home/logic.dart';
import '../my/logic.dart';
import '../promotions/logic.dart';
import 'logic.dart';

class MainBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<MainLogic>(() => MainLogic());
    Get.lazyPut<HomeLogic>(() => HomeLogic());
    Get.lazyPut<PromotionsLogic>(() => PromotionsLogic());
    Get.lazyPut<MyLogic>(() => MyLogic());
  }
}
