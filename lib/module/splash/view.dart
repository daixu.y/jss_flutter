import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'logic.dart';

///启动页
class SplashPage extends StatelessWidget {
  final logic = Get.find<SplashLogic>();
  final state = Get.find<SplashLogic>().state;

  SplashPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Obx(
        () => AnimatedOpacity(
          onEnd: () => logic.jump(),
          opacity: state.opacityLevel.value,
          duration: const Duration(milliseconds: 2000),
        ),
      ),
    );
  }
}
