import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:jss_flutter/app/config/route_config.dart';
import 'package:jss_flutter/utils/log_utils.dart';

import 'state.dart';

class SplashLogic extends GetxController {
  final SplashState state = SplashState();
  var storageBox = GetStorage();

  @override
  void onInit() {
    super.onInit();
    if (null != storageBox.read("token")) {
      String token = storageBox.read("token");
      LogUtils.d("token=$token");
      state.token.value = token;
    }
    lazyInitAnim();
  }

  lazyInitAnim() {
    Future.delayed(const Duration(milliseconds: 200), () {
      state.opacityLevel.value = 1.0;
    });
  }

  void jump() {
    Get.offNamed(state.token.isEmpty ? RouteConfig.login : RouteConfig.main);
  }
}
