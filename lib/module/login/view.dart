import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:jss_flutter/utils/log_utils.dart';
import 'package:jss_flutter/utils/toast_utils.dart';

import 'logic.dart';

///登录
class LoginPage extends StatelessWidget {
  final logic = Get.find<LoginLogic>();
  final state = Get.find<LoginLogic>().state;
  final phoneController = TextEditingController();
  final codeController = TextEditingController();

  LoginPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: Center(
        child: Column(
          children: [
            const Padding(padding: EdgeInsets.only(top: 120)),
            Image.asset("assets/images/logo.png"),
            Padding(
              padding: const EdgeInsets.only(left: 50, top: 50, right: 50),
              child: _inputField("请输入手机号码"),
            ),
            Padding(
              padding: const EdgeInsets.only(left: 50, top: 20, right: 50),
              child: _inputCode(),
            ),
            Padding(
              padding: const EdgeInsets.only(left: 50, top: 50, right: 50),
              child: Row(
                children: [
                  _colorfulCheckBox(),
                  Expanded(
                    child: _protocolText(),
                  ),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 15),
              child: _loginButton(),
            ),
          ],
        ),
      ),
    );
  }

  TextSpan _protocolSpan(String text, Color color, double fontSize, int type) {
    return TextSpan(
      text: text,
      style: TextStyle(
        color: color,
        fontSize: fontSize,
      ),
      recognizer: TapGestureRecognizer()
        ..onTap = () {
          if (type == 1) {
            toast("《用户协议》");
          } else if (type == 2) {
            toast("《隐私政策》");
          }
        },
    );
  }

  RichText _protocolText() {
    return RichText(
      text: TextSpan(
        children: <TextSpan>[
          _protocolSpan("我已阅读并同意", const Color(0xff999999), 12, 0),
          _protocolSpan("《用户协议》", const Color(0xff6096F7), 12, 1),
          _protocolSpan("《隐私政策》", const Color(0xff6096F7), 12, 2),
          _protocolSpan("未注册的手机号将自动注册聚刷刷账号", const Color(0xff999999), 12, 0),
        ],
      ),
    );
  }

  Widget _loginButton() {
    return MaterialButton(
      onPressed: () {
        var phone = phoneController.text;
        var code = codeController.text;
        logic.login(phone, code);
      },
      highlightColor: const Color(0xffFA681E),
      colorBrightness: Brightness.dark,
      splashColor: Colors.grey,
      color: const Color(0xffFD4D0E),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(24),
      ),
      height: 45,
      minWidth: 255,
      child: const Text(
        "登录",
        style: TextStyle(fontSize: 16),
      ),
    );
  }

  Widget _getCodeButton() {
    return TextButton(
      onPressed: () {
        if (state.countdownTime.value == 0) {
          var phone = phoneController.text;
          LogUtils.d("phone=$phone");
          logic.getCode(phone);
        }
      },
      child: Obx(
        () => Text(
          "${state.codeStr}",
          style: const TextStyle(color: Color(0xffFF3300)),
        ),
      ),
    );
  }

  Widget _inputCodeText() {
    return Expanded(
      child: TextField(
        maxLines: 1,
        controller: codeController,
        textAlign: TextAlign.start,
        keyboardType: TextInputType.number,
        decoration: InputDecoration(
          hintText: "请输入验证码",
          contentPadding: const EdgeInsets.fromLTRB(20, 10, 20, 10),
          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(24),
            borderSide: BorderSide.none,
          ),
        ),
        inputFormatters: <TextInputFormatter>[
          // FilteringTextInputFormatter.digitsOnly,
          LengthLimitingTextInputFormatter(6) //限制长度
        ],
      ),
    );
  }

  Widget _inputCode() {
    return Container(
      padding: const EdgeInsets.only(right: 20),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(24),
        color: const Color(0xffEAEAEA),
      ),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          _inputCodeText(),
          _getCodeButton(),
        ],
      ),
    );
  }

  Widget _inputField(String hint) {
    return TextField(
      maxLines: 1,
      textAlign: TextAlign.start,
      controller: phoneController,
      keyboardType: TextInputType.number,
      decoration: InputDecoration(
        hintText: hint,
        contentPadding: const EdgeInsets.fromLTRB(20, 10, 20, 10),
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(24),
          borderSide: BorderSide.none,
        ),
        filled: true,
        fillColor: const Color(0xffEAEAEA),
      ),
      inputFormatters: <TextInputFormatter>[
        // FilteringTextInputFormatter.digitsOnly,
        LengthLimitingTextInputFormatter(11) //限制长度
      ],
    );
  }

  Widget _colorfulCheckBox() {
    return Obx(
      () => Checkbox(
        value: state.isChecked.value,
        shape: const CircleBorder(),
        onChanged: (value) {
          logic.changeChecked(value!);
        },
      ),
    );
  }
}
