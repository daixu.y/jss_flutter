import 'dart:async';

import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:jss_flutter/app/config/route_config.dart';
import 'package:jss_flutter/bean/login_entity.dart';
import 'package:jss_flutter/http/net/http_client.dart';
import 'package:jss_flutter/http/net/http_response.dart';
import 'package:jss_flutter/utils/log_utils.dart';
import 'package:jss_flutter/utils/toast_utils.dart';

import 'state.dart';

class LoginLogic extends GetxController {
  final LoginState state = LoginState();
  Timer? _timer;
  var storageBox = GetStorage();
  var dio = Get.find<HttpClient>();

  void jump() => Get.offNamed(RouteConfig.main);

  void startCountdown() {
    state.countdownTime.value = 60;
    _timer?.cancel();
    _timer = null;
    _timer = Timer.periodic(const Duration(seconds: 1), (call) {
      if (state.countdownTime < 1) {
        _timer?.cancel();
        state.codeStr.value = "获取验证码";
      } else {
        state.countdownTime -= 1;
        state.codeStr.value = "${state.countdownTime}s";
      }
    });
  }

  void changeChecked(bool value) {
    state.isChecked.value = value;
  }

  bool _checkPhone(String phone) {
    if (phone.isEmpty) {
      toast("账号输入为空");
      return false;
    }
    if (phone.length != 11) {
      toast("手机号码长度错误");
      return false;
    }
    return true;
  }

  bool _checkCode(String code) {
    if (code.isEmpty) {
      toast("验证码输入为空");
      return false;
    }
    if (code.length < 4) {
      toast("验证码长度错误");
      return false;
    }
    return true;
  }

  void getCode(String phone) async {
    if (_checkPhone(phone)) {
      var data = {"phone": phone};
      String sendCode = "sms/send_code";

      HttpResponse appResponse = await dio.post(sendCode, data: data);

      if (appResponse.ok) {
        LogUtils.d("==ok==${appResponse.data}");
        startCountdown();
      } else {
        LogUtils.d("==fail==${appResponse.error}");
      }
    }
  }

  @override
  void onClose() {
    _timer?.cancel();
    _timer = null;
    super.onClose();
  }

  bool _checkProtocol() {
    if (state.isChecked.isFalse) {
      toast("请先同意用户协议和隐私政策");
      return false;
    }
    return true;
  }

  void login(String phone, String code) async {
    if (_checkProtocol() && _checkPhone(phone) && _checkCode(code)) {
      String loginCode = "login/phone_code_login";
      var data = {"phone": phone, "code": code};
      HttpResponse appResponse = await dio.post(loginCode, data: data);
      LogUtils.d("response= $appResponse");

      if (appResponse.ok) {
        LogUtils.d("==ok==${appResponse.data}");
        var response = LoginEntity.fromJson(appResponse.data);
        LogUtils.d("data= ${response.data}");
        LogUtils.d("token= ${response.data?.token}");
        toast("登录成功");
        storageBox.write("token", response.data?.token);
        _timer?.cancel();
        _timer = null;
        jump();
      } else {
        LogUtils.d("==fail==${appResponse.error}");
        toast("登录失败${appResponse.error}");
      }
    }
  }
}
