import 'package:get/get.dart';

import 'logic.dart';

class StoreOrderDetailBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => StoreOrderDetailLogic());
  }
}
