import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../../utils/image_utils.dart';
import 'logic.dart';

///商家订单详情
class StoreOrderDetailPage extends StatelessWidget {
  final logic = Get.find<StoreOrderDetailLogic>();
  final state = Get.find<StoreOrderDetailLogic>().state;

  StoreOrderDetailPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: const Text("订单详情"),
        elevation: 0,
        backgroundColor: const Color(0xff4482FF),
      ),
      body: _orderDetailWidget(),
    );
  }

  Widget _orderDetailWidget() {
    return ListView(
      children: [
        Container(
          height: 38,
          // color: const Color(0xff4482FF),
          decoration: BoxDecoration(
            color: const Color(0xff4482FF),
            //边框
            border: Border.all(width: 0, color: const Color(0xff4482FF)),
          ),
          padding: const EdgeInsets.fromLTRB(10, 10, 10, 0),
        ),
        _storeOrderWidget(),
        _storeOrderItemsWidget(),
      ],
    );
  }

  Widget _orderPayInfo() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.end,
      children: const [
        Padding(
          padding: EdgeInsets.fromLTRB(10, 10, 10, 0),
          child: Text(
            "实付款¥50",
            style: TextStyle(color: Color(0xff4482FF), fontSize: 14),
          ),
        )
      ],
    );
  }

  Widget _storeOrderWidget() {
    return Stack(
      children: [
        _storeOrderBgWidget(),
        _storeOrderItems(),
      ],
    );
  }

  Widget _storeOrderItems() {
    return Container(
      padding: const EdgeInsets.all(10),
      margin: const EdgeInsets.fromLTRB(10, 0, 10, 0),
      decoration: const BoxDecoration(
        //背景
        color: Colors.white,
        //设置四周圆角 角度
        borderRadius: BorderRadius.all(Radius.circular(10)),
        //设置四周边框
        // border: Border.all(width: 1, color: Colors.red),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          _orderStatusWidget(),
          const Divider(
            height: 1,
            color: Color(0xffe4e4e4),
          ),
          orderInfoWidget()
        ],
      ),
    );
  }

  Widget orderInfoWidget() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(5, 15, 5, 5),
      child: Row(
        children: [
          Image.asset(
            "assets/images/logo.png",
            width: 30,
            height: 30,
          ),
          const Padding(padding: EdgeInsets.only(left: 10)),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              _memberInfoWidget(),
              const Padding(padding: EdgeInsets.only(top: 10)),
              _memberAddressWidget(),
            ],
          )
        ],
      ),
    );
  }

  Widget _memberInfoWidget() {
    return const Text.rich(
      TextSpan(children: [
        TextSpan(
          text: "张三",
          style: TextStyle(
            color: Color(0xff333333),
            fontSize: 16,
          ),
        ),
        TextSpan(
          text: "18312345678",
          style: TextStyle(
            color: Color(0xff666666),
            fontSize: 14,
          ),
        )
      ]),
    );
  }

  Widget _memberAddressWidget() {
    return const Text(
      "湖南省长沙市岳麓区某某街道某某小区",
      style: TextStyle(fontSize: 14, color: Color(0xff333333)),
    );
  }

  Widget _storeOrderBgWidget() {
    return Column(
      children: [
        Container(
          height: 60,
          color: const Color(0xff4482FF),
        ),
      ],
    );
  }

  Widget _orderStatusWidget() {
    return const Padding(
      padding: EdgeInsets.fromLTRB(5, 5, 5, 15),
      child: Text(
        "待付款",
        style: TextStyle(fontSize: 20, color: Color(0xff333333)),
      ),
    );
  }

  Widget _storeOrderItemsWidget() {
    return Container(
      padding: const EdgeInsets.all(15),
      margin: const EdgeInsets.fromLTRB(10, 10, 10, 0),
      decoration: const BoxDecoration(
        //背景
        color: Colors.white,
        //设置四周圆角 角度
        borderRadius: BorderRadius.all(Radius.circular(10)),
        //设置四周边框
        // border: Border.all(width: 1, color: Colors.red),
      ),
      child: Column(
        children: [
          _orderStoreInfoWidget(),
          const Padding(
            padding: EdgeInsets.fromLTRB(0, 15, 0, 10),
            child: Divider(
              height: 1,
              color: Color(0xffe4e4e4),
            ),
          ),
          _orderGoodsItems(),
          _orderPayInfo(),
          const Padding(
            padding: EdgeInsets.fromLTRB(0, 15, 0, 15),
            child: Divider(
              height: 1,
              color: Color(0xffe4e4e4),
            ),
          ),
        ],
      ),
    );
  }

  Widget _orderGoodsItemWidget() {
    return Container(
      height: 82,
      margin: const EdgeInsets.fromLTRB(10, 5, 10, 0),
      child: Row(
        children: [
          roundImageWidget(
            "https://t7.baidu.com/it/u=1831997705,836992814&fm=193&f=GIF",
            82,
            82,
            radius: 6,
          ),
          _orderGoodsItem(),
        ],
      ),
    );
  }

  Widget _orderGoodsItems() {
    return ListView.builder(
      shrinkWrap: true,
      physics: const NeverScrollableScrollPhysics(),
      itemCount: 5,
      itemBuilder: (context, index) {
        return _orderGoodsItemWidget();
      },
    );
  }

  Widget _orderGoodsItem() {
    return Expanded(
      child: Container(
        padding: const EdgeInsets.only(left: 10),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            _orderGoodsInfo(),
            _orderGoodsSkuName(),
            // _orderDeliveryWidget(),
            _orderGoodsSkuPrice()
          ],
        ),
      ),
    );
  }

  Widget _orderGoodsInfo() {
    return Row(
      children: const [
        Expanded(
          child: Text(
            "入门款/三顿半精品速溶咖啡 冷萃拿铁燕麦拿铁咖啡粉…",
            maxLines: 2,
            overflow: TextOverflow.ellipsis,
            style: TextStyle(fontSize: 14, color: Color(0xff333333)),
          ),
        ),
        Text(
          "X2",
          style: TextStyle(fontSize: 14, color: Color(0xff333333)),
        )
      ],
    );
  }

  Widget _orderGoodsSkuName() {
    return const Text(
      "36颗装",
      maxLines: 1,
      overflow: TextOverflow.ellipsis,
      style: TextStyle(
        fontSize: 12,
        color: Color(0xff999999),
      ),
    );
  }

  Widget _orderGoodsSkuPrice() {
    return const Text(
      "¥50",
      style: TextStyle(
        fontSize: 14,
        color: Color(0xff333333),
      ),
    );
  }

  Widget _orderStoreInfoWidget() {
    return Row(
      children: [
        Image.asset(
          "assets/images/logo.png",
          width: 18,
          height: 18,
        ),
        const Padding(padding: EdgeInsets.only(left: 10)),
        const Text(
          "三顿半咖啡屋",
          style: TextStyle(fontSize: 16, color: Color(0xff333333)),
        )
      ],
    );
  }
}
