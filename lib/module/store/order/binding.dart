import 'package:get/get.dart';

import 'list/logic.dart';
import 'logic.dart';

class StoreOrderBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<StoreOrderLogic>(() => StoreOrderLogic());
    Get.lazyPut<StoreOrderListLogic>(() => StoreOrderListLogic());
  }
}
