import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:jss_flutter/bean/order_list_entity.dart';
import 'package:jss_flutter/utils/log_utils.dart';

import '../../../../utils/image_utils.dart';
import '../../../../widgets/indicator/waterdrop_header.dart';
import '../../../../widgets/smart_refresher.dart';
import '../../../../widgets/smart_widget.dart';
import 'logic.dart';

class StoreOrderListPage2 extends StatefulWidget {
  final int type;

  const StoreOrderListPage2({Key? key, required this.type}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _StoreOrderListState2();
}

class _StoreOrderListState2 extends State<StoreOrderListPage2>
    with AutomaticKeepAliveClientMixin {
  final logic = Get.find<StoreOrderListLogic>();

  final RefreshController _refreshController = RefreshController(
    initialRefresh: true,
  );

  @override
  void initState() {
    super.initState();
    LogUtils.e("initState");
    logic.state.page.value = 1;
    logic.getOrderList(widget.type, logic.state.page.value, _refreshController);
  }

  ///加载状态  1加载成功 2下拉加载数据为空 3上拉加载数据为空 4加载失败
  void _onRefresh() async {
    LogUtils.e("_onRefresh");
    logic.state.page.value = 1;
    if (logic.state.loadState.value == 3) {
      _refreshController.resetNoData();
    }
    logic.getOrderList(widget.type, logic.state.page.value, _refreshController);
  }

  void _onLoading() async {
    logic.getOrderList(widget.type, logic.state.page.value, _refreshController);
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return GetBuilder<StoreOrderListLogic>(builder: (logic) {
      return Scaffold(
        backgroundColor: const Color(0xfff5f5f5),
        body: SizedBox(
          width: double.infinity,
          height: double.infinity,
          child: Stack(
            alignment: Alignment.center,
            children: [
              // 未加载前显示的动画，加载之后需要隐藏
              Obx(
                () => loadingWidget(logic.state.loadState.value == 0),
              ),
              Obx(
                () => _smartRefresherBody(logic.state.loadState.value == 1 ||
                    logic.state.loadState.value == 3),
              ),
              //加载数据为空的页面
              Obx(
                () => loadEmptyWidget(logic.state.loadState.value == 2),
              ),
              //加载出错的页面
              Obx(
                () => loadErrorWidget(logic.state.loadState.value == 4),
              ),
            ],
          ),
        ),
      );
    });
  }

  Widget _smartRefresherBody(bool visible) {
    return Obx(
      () => Visibility(
        visible: visible,
        child: SmartRefresher(
          enablePullDown: true,
          enablePullUp: true,
          header: const WaterDropHeader(),
          footer: customFooter(),
          controller: _refreshController,
          onRefresh: _onRefresh,
          onLoading: _onLoading,
          child: ListView.separated(
            itemCount: logic.state.items.length,
            itemBuilder: (context, index) {
              return _buildItem(logic.state.items[index], index);
            },
            separatorBuilder: (BuildContext context, int index) {
              return const VerticalDivider(
                width: 10,
                color: Color(0xFFFFFFFF),
              );
            },
          ),
        ),
      ),
    );
  }

  Widget _buildItem(OrderListData item, int index) {
    return Container(
      // color: Colors.white,
      margin: const EdgeInsets.fromLTRB(10, 10, 10, 0),
      decoration: const BoxDecoration(
        //背景
        color: Colors.white,
        //设置四周圆角 角度
        borderRadius: BorderRadius.all(Radius.circular(10.0)),
      ),
      child: Column(
        children: [
          _orderMemberInfo(item),
          _orderGoodsItems(item.goodsList, index),
          _orderPayInfo(item)
        ],
      ),
    );
  }

  Widget _orderMemberInfo(OrderListData item) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(10, 10, 10, 10),
      child: Row(
        children: [
          circleImageWidget(item.member!.headimgurl, 20, 20),
          Text(
            item.member!.nickname,
            style: const TextStyle(
              fontSize: 14,
              color: Color(0xff333333),
            ),
          ),
          const Spacer(),
          Text(
            item.statusText,
            style: const TextStyle(
              fontSize: 14,
              color: Color(0xff6096F7),
            ),
          ),
        ],
      ),
    );
  }

  Widget _orderGoodsItems(List<OrderListDataGoodsList>? goodsList, int index2) {
    return ListView.separated(
      shrinkWrap: true,
      physics: const NeverScrollableScrollPhysics(),
      itemCount: goodsList!.length,
      itemBuilder: (context, index) {
        return _orderGoodsItemWidget(goodsList[index], index);
      },
      separatorBuilder: (BuildContext context, int index) {
        return const VerticalDivider(
          width: 10,
          color: Color(0xFFFFFFFF),
        );
      },
    );
  }

  Widget _orderGoodsItemWidget(OrderListDataGoodsList goodsInfo, int index) {
    return Container(
      height: 80,
      margin: const EdgeInsets.fromLTRB(10, 10, 10, 0),
      child: Row(
        children: [
          roundImageWidget(goodsInfo.goodsImage, 80, 80, radius: 6),
          _orderGoodsItem(goodsInfo),
        ],
      ),
    );
  }

  Widget _orderGoodsItem(OrderListDataGoodsList goodsInfo) {
    return Expanded(
      child: Container(
        margin: const EdgeInsets.fromLTRB(10, 0, 0, 0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            _orderGoodsInfo(goodsInfo),
            _orderGoodsSkuName(goodsInfo),
            _orderGoodsSkuPrice(goodsInfo),
          ],
        ),
      ),
    );
  }

  Widget _orderGoodsSkuName(OrderListDataGoodsList goodsInfo) {
    return Text(
      goodsInfo.skuName,
      style: const TextStyle(
        fontSize: 14,
        color: Color(0xff999999),
      ),
    );
  }

  Widget _orderGoodsInfo(OrderListDataGoodsList goodsInfo) {
    return Row(
      children: [
        Text(
          goodsInfo.goodsName,
          maxLines: 2,
          style: const TextStyle(
            fontSize: 14,
            color: Color(0xff333333),
          ),
        ),
        const Spacer(),
        Text(
          "X${goodsInfo.num}",
          style: const TextStyle(
            fontSize: 14,
            color: Color(0xff333333),
          ),
        ),
      ],
    );
  }

  Widget _orderPayInfo(OrderListData item) {
    return Row(
      children: [
        const Spacer(),
        Padding(
          padding: const EdgeInsets.fromLTRB(10, 10, 10, 10),
          child: Text("实付款${item.unpaid}"),
        )
      ],
    );
  }

  Widget _orderGoodsSkuPrice(OrderListDataGoodsList goodsInfo) {
    return Text(
      "¥${goodsInfo.price}",
      style: const TextStyle(
        fontSize: 14,
        color: Color(0xff333333),
      ),
    );
  }

  @override
  bool get wantKeepAlive => true;
}
