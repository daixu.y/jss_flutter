import 'package:get/get.dart';
import 'package:jss_flutter/app/config/route_config.dart';
import 'package:jss_flutter/bean/order_list_entity.dart';
import 'package:jss_flutter/http/net/http_client.dart';
import 'package:jss_flutter/http/net/http_response.dart';
import 'package:jss_flutter/utils/log_utils.dart';
import 'package:jss_flutter/widgets/smart_refresher.dart';

import 'state.dart';

class StoreOrderListLogic extends GetxController {
  final StoreOrderListState state = StoreOrderListState();
  final HttpClient dio = Get.find<HttpClient>();

  void getOrderList(int status, int page, RefreshController refreshController) async {
    var data = {"status": status, "type": 0, "page": page};
    // shop_order/order_list

    HttpResponse appResponse =
        await dio.get("shop_order/order_list", queryParameters: data);
    var response = OrderListEntity.fromJson(appResponse.data);

    if (appResponse.ok) {
      ///加载状态  1加载成功 2下拉加载数据为空 3上拉加载数据为空 4加载失败
      if (null != response.data) {
        if (state.page.value == 1) {
          state.items.clear();
          state.loadState.value = response.data?.isNotEmpty == true ? 1 : 2;
          refreshController.refreshCompleted();
        } else {
          state.loadState.value = response.data?.isNotEmpty == true ? 1 : 3;
          if (response.data?.isNotEmpty == true) {
            state.loadState.value = 1;
            refreshController.loadComplete();
          } else {
            state.loadState.value == 3;
            refreshController.loadNoData();
          }
        }
        state.page.value++;
        state.items.addAll(response.data!);
      } else {
        if (state.page.value == 1) {
          state.items.clear();
          state.loadState.value = 2;
          refreshController.refreshCompleted();
        } else {
          state.loadState.value = 3;
          refreshController.loadNoData();
        }
      }
    } else {
      LogUtils.e("==fail==${appResponse.error}");
      state.loadState.value = 4;

      if (state.page.value == 1) {
        refreshController.refreshCompleted();
      } else {
        refreshController.loadComplete();
      }
    }
  }

  void jump2Detail() => Get.toNamed(RouteConfig.storeOrderDetail);
}
