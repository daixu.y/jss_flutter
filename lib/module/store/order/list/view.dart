import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:jss_flutter/bean/order_list_entity.dart';

import '../../../../utils/image_utils.dart';
import '../../../../widgets/smart_refresher.dart';
import '../../../../widgets/smart_widget.dart';
import 'logic.dart';

///商家订单列表
class StoreOrderListPage extends GetView<StoreOrderListLogic> {
  final int type;

  StoreOrderListPage({Key? key, required this.type}) : super(key: key);

  final RefreshController _refreshController = RefreshController(
    initialRefresh: true,
  );

  ///加载状态  1加载成功 2下拉加载数据为空 3上拉加载数据为空 4加载失败
  void _onRefresh() async {
    controller.state.page.value = 1;
    if (controller.state.loadState.value == 3) {
      _refreshController.resetNoData();
    }
    controller.getOrderList(
      type,
      controller.state.page.value,
      _refreshController,
    );
  }

  void _onLoading() async {
    controller.getOrderList(
      type,
      controller.state.page.value,
      _refreshController,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color(0xfff5f5f5),
      body: SizedBox(
        width: double.infinity,
        height: double.infinity,
        child: Stack(
          alignment: Alignment.center,
          children: [
            // 未加载前显示的动画，加载之后需要隐藏
            Obx(
              () => loadingWidget(controller.state.loadState.value == 0),
            ),
            //上拉刷新、下拉加载控件
            Obx(
              () => _smartRefresherBody(true),
            ),
            // Obx(
            //   () => _smartRefresherBody(controller.state.loadState.value == 1 ||
            //       controller.state.loadState.value == 3),
            // ),
            //加载数据为空的页面
            Obx(
              () => loadEmptyWidget(controller.state.loadState.value == 2),
            ),
            //加载出错的页面
            Obx(
              () => loadErrorWidget(controller.state.loadState.value == 4),
            ),
          ],
        ),
      ),
    );
  }

  Widget _smartRefresherBody(bool visible) {
    return SmartRefresher(
        enablePullDown: true,
        enablePullUp: true,
        // header: const WaterDropHeader(),
        header: customHeader(),
        footer: customFooter(),
        controller: _refreshController,
        onRefresh: _onRefresh,
        onLoading: _onLoading,
        child: ListView.builder(
          itemCount: controller.state.items.length,
          itemBuilder: (context, index) {
            return _buildItem(controller.state.items[index], index);
          },
        ),
      );
  }

  Widget _buildItem(OrderListData item, int index) {
    return Container(
      // color: Colors.white,
      margin: const EdgeInsets.fromLTRB(10, 10, 10, 0),
      decoration: const BoxDecoration(
        //背景
        color: Colors.white,
        //设置四周圆角 角度
        borderRadius: BorderRadius.all(Radius.circular(10)),
      ),
      child: Column(
        children: [
          _orderMemberInfo(item),
          _orderGoodsItems(item),
          _orderPayInfo(item),
          _orderOptionWidget(item)
        ],
      ),
    );
  }

  Widget _orderMemberInfo(OrderListData item) {
    return Padding(
      padding: const EdgeInsets.all(10),
      child: Row(
        children: [
          circleImageWidget(item.member!.headimgurl, 20, 20),
          Expanded(
            child: Text(
              " ${item.member!.nickname}",
              maxLines: 1,
              overflow: TextOverflow.ellipsis,
              style: const TextStyle(
                fontSize: 14,
                color: Color(0xff333333),
              ),
            ),
          ),
          Text(
            item.statusText,
            maxLines: 1,
            overflow: TextOverflow.ellipsis,
            style: const TextStyle(
              fontSize: 14,
              color: Color(0xff6096F7),
            ),
          ),
        ],
      ),
    );
  }

  Widget _orderGoodsItems(OrderListData item) {
    return ListView.builder(
      shrinkWrap: true,
      physics: const NeverScrollableScrollPhysics(),
      itemCount: item.goodsList!.length,
      itemBuilder: (context, index) {
        return _orderGoodsItemWidget(item, item.goodsList![index]);
      },
    );
  }

  Widget _orderGoodsItemWidget(
      OrderListData item, OrderListDataGoodsList goodsInfo) {
    return Container(
      height: 82,
      margin: const EdgeInsets.fromLTRB(10, 5, 10, 0),
      child: Row(
        children: [
          roundImageWidget(goodsInfo.goodsImage, 82, 82, radius: 6),
          _orderGoodsItem(item, goodsInfo),
        ],
      ),
    );
  }

  Widget _orderGoodsItem(OrderListData item, OrderListDataGoodsList goodsInfo) {
    return Expanded(
      child: Container(
        margin: const EdgeInsets.fromLTRB(10, 0, 0, 0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            _orderGoodsInfo(goodsInfo),
            _orderGoodsSkuName(goodsInfo),
            _orderDeliveryWidget(item),
            _orderGoodsSkuPrice(goodsInfo),
          ],
        ),
      ),
    );
  }

  Widget _orderDeliveryWidget(OrderListData item) {
    return Container(
      margin: const EdgeInsets.only(top: 1, bottom: 1),
      decoration: BoxDecoration(
        color: item.deliveryWay == 1
            ? const Color(0xffff3300)
            : item.deliveryWay == 2
                ? const Color(0xffffba12)
                : const Color(0xff2ec3b8),
        borderRadius: const BorderRadius.all(Radius.circular(2)),
      ),
      padding: const EdgeInsets.fromLTRB(3, 1, 3, 1),
      child: Text(
        item.deliveryWay == 1
            ? "包邮"
            : item.deliveryWay == 2
                ? "门店自提"
                : "邮费到付",
        style: const TextStyle(
          fontSize: 10,
          color: Colors.white,
        ),
      ),
    );
  }

  Widget _orderGoodsSkuName(OrderListDataGoodsList goodsInfo) {
    return Text(
      goodsInfo.skuName,
      maxLines: 1,
      overflow: TextOverflow.ellipsis,
      style: const TextStyle(
        fontSize: 14,
        color: Color(0xff999999),
      ),
    );
  }

  Widget _orderGoodsInfo(OrderListDataGoodsList goodsInfo) {
    return Row(
      children: [
        Expanded(
          child: Text(
            goodsInfo.goodsName,
            maxLines: 2,
            overflow: TextOverflow.ellipsis,
            style: const TextStyle(
              fontSize: 14,
              color: Color(0xff333333),
            ),
          ),
        ),
        Text(
          "X${goodsInfo.num}",
          style: const TextStyle(
            fontSize: 14,
            color: Color(0xff333333),
          ),
        ),
      ],
    );
  }

  Widget _orderPayInfo(OrderListData item) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.end,
      children: [
        Padding(
          padding: const EdgeInsets.fromLTRB(10, 10, 10, 0),
          child: Text("实付款${item.unpaid}"),
        )
      ],
    );
  }

  Widget _orderGoodsSkuPrice(OrderListDataGoodsList goodsInfo) {
    return Text(
      "¥${goodsInfo.price}",
      maxLines: 1,
      overflow: TextOverflow.ellipsis,
      style: const TextStyle(
        fontSize: 14,
        color: Color(0xff333333),
      ),
    );
  }

  Widget _orderOptionWidget(OrderListData item) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.end,
      children: [
        _orderShipWidget(item),
        _orderDetailWidget(item),
      ],
    );
  }

  Widget _orderShipWidget(OrderListData item) {
    return Visibility(
      visible: item.status == 2 && item.deliveryWay != 2,
      child: OutlinedButton(
        style: OutlinedButton.styleFrom(
          minimumSize: const Size(78, 32),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(20),
          ),
          side: const BorderSide(width: 1, color: Color(0xff4482ff)),
        ),
        onPressed: () {},
        child: const Text(
          "立即发货",
          style: TextStyle(
            fontSize: 14,
            color: Color(0xff4482ff),
          ),
        ),
      ),
    );
  }

  Widget _orderDetailWidget(OrderListData item) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
      child: OutlinedButton(
        style: OutlinedButton.styleFrom(
          minimumSize: const Size(78, 32),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(20),
          ),
          side: const BorderSide(width: 1, color: Color(0xffcecece)),
        ),
        onPressed: () {
          controller.jump2Detail();
        },
        child: const Text(
          "订单详情",
          style: TextStyle(
            fontSize: 14,
            color: Color(0xff999999),
          ),
        ),
      ),
    );
  }
}
