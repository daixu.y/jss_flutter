import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';

import 'list/view.dart';
import 'logic.dart';

///商家订单
class StoreOrderPage extends StatelessWidget {
  final logic = Get.find<StoreOrderLogic>();
  final state = Get.find<StoreOrderLogic>().state;

  StoreOrderPage({Key? key}) : super(key: key);

  final List<Tab> mTabs = <Tab>[
    const Tab(text: '全部'),
    const Tab(text: '待付款'),
    const Tab(text: '待发货'),
    const Tab(text: '已发货'),
  ];

  @override
  Widget build(BuildContext context) {
    int type = 0;
    if (null != Get.arguments) {
      type = Get.arguments;
    }
    var current = type;
    return DefaultTabController(
      length: mTabs.length,
      initialIndex: current, // 默认选中项
      child: Scaffold(
        appBar: AppBar(
          iconTheme: const IconThemeData(
            color: Colors.black, //修改颜色
          ),
          titleTextStyle: const TextStyle(color: Colors.black, fontSize: 18),
          systemOverlayStyle: SystemUiOverlayStyle.dark,
          backgroundColor: Colors.white,
          title: const Text('我的订单'),
          elevation: 0,
          centerTitle: true,
          bottom: TabBar(
            tabs: mTabs,
            indicatorColor: const Color(0xff4482FF),
            labelColor: const Color(0xff333333),
            indicatorSize: TabBarIndicatorSize.label,
          ),
        ),
        body: TabBarView(
          children: [
            StoreOrderListPage(type: 0),
            StoreOrderListPage(type: 1),
            StoreOrderListPage(type: 2),
            StoreOrderListPage(type: 3),
          ],
        ),
      ),
    );
  }

}
