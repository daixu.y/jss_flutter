import 'package:get/get.dart';

import 'logic.dart';

class BillDetailBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => BillDetailLogic());
  }
}
