import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:jss_flutter/bean/bill_list_entity.dart';
import 'package:jss_flutter/utils/date_utils.dart';
import 'package:jss_flutter/widgets/smart_refresher.dart';
import 'package:jss_flutter/widgets/smart_widget.dart';

import 'logic.dart';

///商家账单详情
class BillDetailPage extends StatelessWidget {
  final logic = Get.find<BillDetailLogic>();
  final state = Get.find<BillDetailLogic>().state;

  BillDetailPage({Key? key}) : super(key: key);

  ///加载状态  1加载成功 2下拉加载数据为空 3上拉加载数据为空 4加载失败
  void _onRefresh() async {
    state.page.value = 1;
    logic.getBillList(state.page.value, isRefresh: true);
  }

  void _onLoading() async {
    logic.getBillList(state.page.value);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        iconTheme: const IconThemeData(
          color: Colors.black, //修改颜色
        ),
        titleTextStyle: const TextStyle(color: Colors.black, fontSize: 18),
        systemOverlayStyle: SystemUiOverlayStyle.dark,
        backgroundColor: Colors.white,
        centerTitle: true,
        title: const Text("账单列表"),
        elevation: 0,
      ),
      body: SizedBox(
        width: double.infinity,
        height: double.infinity,
        child: Stack(
          alignment: Alignment.center,
          children: [
            // 未加载前显示的动画，加载之后需要隐藏
            Obx(
              () => loadingWidget(state.loadState.value == 0),
            ),
            Obx(
              () => _billWidget(
                state.loadState.value == 1 || state.loadState.value == 3,
              ),
            ),
            Obx(
              () => loadEmptyWidget(state.loadState.value == 2),
            ),
            Obx(
              () => loadErrorWidget(state.loadState.value == 4),
            ),
          ],
        ),
      ),
      backgroundColor: const Color(0xfff5f5f5),
    );
  }

  Widget _billWidget(bool visible) {
    return Visibility(
      visible: visible,
      child: SmartRefresher(
        enablePullDown: true,
        enablePullUp: true,
        // header: const WaterDropHeader(),
        header: customHeader(),
        footer: customFooter(),
        controller: logic.refreshController,
        onRefresh: _onRefresh,
        onLoading: _onLoading,
        child: ListView.builder(
          itemCount: state.items.length,
          itemBuilder: (context, index) {
            return _buildItem(state.items[index], index);
          },
        ),
      ),
    );
  }

  Widget _buildItem(BillListData item, int index) {
    return Container(
      margin: const EdgeInsets.fromLTRB(10, 10, 10, 0),
      padding: const EdgeInsets.all(10),
      decoration: const BoxDecoration(
        //背景
        color: Colors.white,
        //设置四周圆角 角度
        borderRadius: BorderRadius.all(Radius.circular(10)),
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          billItem(item),
          billItem2(item),
        ],
      ),
    );
  }

  Widget billItem(BillListData item) {
    return Row(
      children: [
        Expanded(
          child: Text(
            item.remark.isNotEmpty ? item.remark : item.typeText,
            style: const TextStyle(
              fontSize: 14,
              color: Color(0xff333333),
            ),
            maxLines: 2,
            overflow: TextOverflow.ellipsis,
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(left: 10),
          child: Text(
            "+${item.money}",
            style: const TextStyle(
              fontSize: 16,
              color: Color(0xff333333),
            ),
          ),
        ),
      ],
    );
  }

  Widget billItem2(BillListData item) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(0, 6, 0, 0),
      child: Row(
        children: [
          Expanded(
            child: Text(
              DateFormatUtils.formatTime(
                "yyyy-MM-dd'T'HH:mm:ss.SSSSSS'Z'",
                "yyyy-MM-dd HH:mm:ss",
                item.createdAt!,
              ),
              style: const TextStyle(
                fontSize: 12,
                color: Color(0xff999999),
              ),
            ),
          ),
          Text(
            "余额${item.newMoney}",
            style: const TextStyle(
              fontSize: 12,
              color: Color(0xff999999),
            ),
          ),
        ],
      ),
    );
  }
}
