import 'package:get/get.dart';

import 'logic.dart';

class StoreRevenueBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => StoreRevenueLogic());
  }
}
