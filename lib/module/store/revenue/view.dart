import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:jss_flutter/widgets/smart_widget.dart';

import 'logic.dart';

/// 店铺营收
class StoreRevenuePage extends StatelessWidget {
  final logic = Get.find<StoreRevenueLogic>();
  final state = Get.find<StoreRevenueLogic>().state;

  StoreRevenuePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        iconTheme: const IconThemeData(
          color: Colors.black, //修改颜色
        ),
        titleTextStyle: const TextStyle(color: Colors.black, fontSize: 18),
        systemOverlayStyle: SystemUiOverlayStyle.dark,
        backgroundColor: Colors.white,
        centerTitle: true,
        title: const Text("店铺营收"),
        elevation: 0,
      ),
      body: SizedBox(
        width: double.infinity,
        height: double.infinity,
        child: Stack(
          alignment: Alignment.center,
          children: [
            // 未加载前显示的动画，加载之后需要隐藏
            Obx(
              () => loadingWidget(state.loadState.value == 0),
            ),
            Obx(
              () => _revenueWidget(state.loadState.value == 1),
            ),
            Obx(
              () => loadEmptyWidget(state.loadState.value == 2),
            ),
            Obx(
              () => loadErrorWidget(state.loadState.value == 3),
            ),
          ],
        ),
      ),
      backgroundColor: const Color(0xfff5f5f5),
    );
  }

  Widget _revenueWidget(bool visible) {
    return Visibility(
      visible: visible,
      child: Column(
        children: [
          _revenueDetailWidget(),
          _revenueItemWidget("账单明细", () => logic.jumpBillDetail()),
          const Divider(
            height: 1,
            color: Color(0xfff5f5f5),
          ),
          _revenueItemWidget("提现记录", () => logic.jumpWithdrawRecord()),
        ],
      ),
    );
  }

  Widget _revenueDetailWidget() {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 10),
      margin: const EdgeInsets.symmetric(vertical: 10),
      color: Colors.white,
      child: Column(
        children: [
          _balanceWidget(),
          const Divider(
            height: 1,
            color: Color(0xfff5f5f5),
          ),
          _revenueInfoWidget(),
        ],
      ),
    );
  }

  Widget _revenueInfoWidget() {
    return SizedBox(
      height: 80,
      child: Row(
        children: [
          _settlementWidget(
              "提现中金额(元)", state.storeWallet.value.shopAccountFrozen),
          const Padding(
            padding: EdgeInsets.fromLTRB(0, 0, 15, 0),
            child: VerticalDivider(
              width: 1,
              color: Color(0xfff5f5f5),
            ),
          ),
          _settlementWidget(
              "待结算金额(元)", state.storeWallet.value.shopNoSettlement),
        ],
      ),
    );
  }

  Widget _settlementWidget(String title, String amount) {
    return Expanded(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text(
            title,
            style: const TextStyle(
              color: Color(0xff999999),
              fontSize: 14,
            ),
          ),
          Padding(
            padding: const EdgeInsets.fromLTRB(0, 6, 0, 0),
            child: Text(
              amount,
              style: const TextStyle(
                color: Color(0xff333333),
                fontSize: 16,
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _balanceWidget() {
    return SizedBox(
      height: 80,
      child: Row(
        children: [
          _balanceItemWidget(),
          const Spacer(),
          _withdrawButton(),
        ],
      ),
    );
  }

  Widget _balanceItemWidget() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        const Text(
          "可提现金额(元)",
          style: TextStyle(
            color: Color(0xff999999),
            fontSize: 14,
          ),
        ),
        Padding(
          padding: const EdgeInsets.fromLTRB(0, 6, 0, 0),
          child: Text(
            state.storeWallet.value.shopMoney,
            style: const TextStyle(
              color: Color(0xff333333),
              fontSize: 16,
            ),
          ),
        ),
      ],
    );
  }

  Widget _withdrawButton() {
    return MaterialButton(
      onPressed: () => {},
      color: const Color(0xff6096f7),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(4),
      ),
      elevation: 0,
      height: 28,
      minWidth: 64,
      child: const Text(
        "提现",
        style: TextStyle(
          color: Colors.white,
          fontSize: 14,
        ),
      ),
    );
  }

  Widget _revenueItemWidget(String title, GestureTapCallback onTap) {
    return GestureDetector(
      onTap: onTap,
      child: Container(
        padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
        color: Colors.white,
        height: 45,
        child: Row(
          children: [
            Text(title),
            const Spacer(),
            Padding(
              padding: const EdgeInsets.only(left: 5, right: 5),
              child: Image.asset(
                "assets/images/icon_next_black.webp",
                width: 7,
                height: 12,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
