import 'package:get/get.dart';
import 'package:jss_flutter/bean/store_wallet_entity.dart';

class StoreRevenueState {
  var storeWallet = StoreWalletData().obs;

  /// 0 未加载 1 加载成功 2 加载失败
  final loadState = 0.obs;

  StoreRevenueState() {
    ///Initialize variables
  }
}
