import 'package:get/get.dart';
import 'package:jss_flutter/app/config/route_config.dart';
import 'package:jss_flutter/bean/store_wallet_entity.dart';
import 'package:jss_flutter/http/net/http_client.dart';
import 'package:jss_flutter/http/net/http_response.dart';
import 'package:jss_flutter/utils/log_utils.dart';

import 'state.dart';

class StoreRevenueLogic extends GetxController {
  final StoreRevenueState state = StoreRevenueState();
  final HttpClient dio = Get.find<HttpClient>();

  /// 可 async 拉取数据
  /// 可触发展示交互组件
  /// onInit 之后
  @override
  void onReady() {
    super.onReady();
    getStoreWallet();
  }

  void getStoreWallet() async {
    // shop/shop_wallet
    HttpResponse appResponse = await dio.get("shop/shop_wallet");
    var response = StoreWalletEntity.fromJson(appResponse.data);

    if (appResponse.ok) {
      if (null != response.data) {
        state.storeWallet.value = response.data!;
        state.loadState.value = 1;
      } else {
        state.loadState.value = 2;
      }
    } else {
      LogUtils.e("==fail==${appResponse.error}");
      state.loadState.value = 3;
    }
  }

  void jumpBillDetail() => Get.toNamed(RouteConfig.storeBillDetail);

  void jumpWithdrawRecord() => Get.toNamed(RouteConfig.storeWithdrawRecord);
}
