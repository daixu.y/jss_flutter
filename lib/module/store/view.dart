import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:jss_flutter/utils/toast_utils.dart';
import 'package:jss_flutter/widgets/smart_widget.dart';

import 'logic.dart';

///商家管理
class StorePage extends StatelessWidget {
  final logic = Get.find<StoreLogic>();
  final state = Get.find<StoreLogic>().state;

  StorePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: const Text("商家管理"),
        elevation: 0,
        automaticallyImplyLeading: false,
        backgroundColor: const Color(0xff4482FF),
      ),
      body: SizedBox(
        width: double.infinity,
        height: double.infinity,
        child: Stack(
          alignment: Alignment.center,
          children: [
            // 未加载前显示的动画，加载之后需要隐藏
            Obx(
              () => loadingWidget(state.loadState.value == 0),
            ),
            Obx(
              () => _storeWidget(state.loadState.value == 1),
            ),
            Obx(
              () => loadEmptyWidget(state.loadState.value == 2),
            ),
            Obx(
              () => loadErrorWidget(state.loadState.value == 3),
            ),
          ],
        ),
      ),
      backgroundColor: const Color(0xfff5f5f5),
    );
  }

  Widget _storeWidget(bool visible) {
    return Visibility(
      visible: visible,
      child: ListView(
        children: [
          Container(
            height: 158,
            decoration: BoxDecoration(
              color: const Color(0xff4482FF),
              //边框
              border: Border.all(width: 0, color: const Color(0xff4482FF)),
            ),
            padding: const EdgeInsets.fromLTRB(10, 30, 10, 0),
            child: Column(
              children: [
                _storeBasicInfo(),
                _storeExpiryTime(),
              ],
            ),
          ),
          _storeOrderWidget(),
          _storeAssetsWidget(),
          _storeGoodsManageWidget(),
          _storeOtherManageWidget(),
        ],
      ),
    );
  }

  Widget _storeAssetsWidget() {
    return Container(
      // color: Colors.red,
      height: 132,
      margin: const EdgeInsets.fromLTRB(10, 10, 10, 10),
      padding: const EdgeInsets.all(10),
      decoration: const BoxDecoration(
        //背景
        color: Colors.white,
        //设置四周圆角 角度
        borderRadius: BorderRadius.all(Radius.circular(10)),
      ),
      child: Column(
        children: [
          _assetsLabel(),
          const Padding(
            padding: EdgeInsets.only(top: 15, bottom: 15),
            child: Divider(
              color: Color(0xffe4e4e4),
              height: 1,
            ),
          ),
          _assetsItems(),
        ],
      ),
    );
  }

  Widget _assetsLabel() {
    return Row(
      children: [
        Image.asset(
          "assets/images/icon_assets.webp",
          width: 16,
          height: 16,
        ),
        const Padding(padding: EdgeInsets.only(left: 3)),
        const Text("我的资产(元)"),
        const Spacer(),
        InkWell(
          onTap: () => logic.jump2Revenue(),
          child: Row(
            children: [
              Text(
                state.storeWallet.value.shopMoney,
                style: const TextStyle(
                  fontSize: 18,
                  color: Color(0xFF333333),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 10, right: 10),
                child: Image.asset(
                  "assets/images/icon_next_black.webp",
                  width: 7,
                  height: 12,
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }

  Widget _assetsItems() {
    return Row(
      children: [
        _assetsItem(
          "昨日收益",
          state.storeWallet.value.yesterdayMoney,
          () => toast("哈哈哈0"),
        ),
        _assetsItem(
          "累计收益",
          state.storeWallet.value.totalMoney,
          () => toast("哈哈哈1"),
        ),
        _assetsItem(
          "易货券",
          state.storeWallet.value.barterMoney,
          () => toast("哈哈哈2"),
        ),
      ],
    );
  }

  Widget _assetsItem(String name, String value, GestureTapCallback onTap) {
    return Expanded(
      flex: 1,
      child: GestureDetector(
        onTap: onTap,
        child: Column(
          children: [
            Text(
              name,
              style: const TextStyle(
                fontSize: 14,
                color: Color(0xff666666),
              ),
              maxLines: 1,
              overflow: TextOverflow.ellipsis,
            ),
            const Padding(padding: EdgeInsets.only(top: 10)),
            Text(
              value,
              style: const TextStyle(
                fontSize: 14,
                color: Color(0xff333333),
              ),
              maxLines: 1,
              overflow: TextOverflow.ellipsis,
            )
          ],
        ),
      ),
    );
  }

  Widget _storeOrderBgWidget() {
    return Column(
      children: [
        Container(
          height: 40,
          color: const Color(0xff4482FF),
        ),
        const SizedBox(
          height: 48,
        ),
      ],
    );
  }

  Widget _storeOrderWidget() {
    return Stack(
      children: [
        _storeOrderBgWidget(),
        _storeOrderItems(),
      ],
    );
  }

  _orderLabel() {
    return Row(
      children: [
        const Text(
          "我的订单",
          style: TextStyle(
            fontSize: 14,
            color: Color(0xff333333),
          ),
        ),
        const Spacer(),
        Padding(
          padding: const EdgeInsets.only(left: 10, right: 10),
          child: InkWell(
            onTap: () => logic.jumpOrderList(0),
            child: const Text(
              "全部 >",
              style: TextStyle(
                fontSize: 14,
                color: Color(0xff999999),
              ),
            ),
          ),
        )
      ],
    );
  }

  Widget _storeOrderItems() {
    return Container(
      // color: Colors.red,
      padding: const EdgeInsets.all(10),
      margin: const EdgeInsets.fromLTRB(10, 0, 10, 0),
      decoration: const BoxDecoration(
        //背景
        color: Colors.white,
        //设置四周圆角 角度
        borderRadius: BorderRadius.all(Radius.circular(10)),
        //设置四周边框
        // border: Border.all(width: 1, color: Colors.red),
      ),
      height: 100,
      child: Column(
        children: [
          _orderLabel(),
          const Padding(padding: EdgeInsets.only(top: 12)),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              _orderItem(
                "assets/images/logo.png",
                "待付款",
                () => logic.jumpOrderList(1),
              ),
              _orderItem(
                "assets/images/logo.png",
                "待发货",
                () => logic.jumpOrderList(2),
              ),
              _orderItem(
                "assets/images/logo.png",
                "已发货",
                () => logic.jumpOrderList(3),
              ),
              _orderItem(
                "assets/images/logo.png",
                "退款/售后",
                () => logic.jumpAfterList(),
              ),
            ],
          )
        ],
      ),
    );
  }

  Widget _orderItem(String name, String title, GestureTapCallback onTap) {
    return GestureDetector(
      onTap: onTap,
      child: Column(
        children: [
          Image.asset(
            name,
            width: 22,
            height: 22,
            fit: BoxFit.cover,
          ),
          const Padding(padding: EdgeInsets.only(top: 7)),
          Text(
            title,
            style: const TextStyle(
              fontSize: 12,
              color: Color(0xff333333),
            ),
          )
        ],
      ),
    );
  }

  Widget _storeBasicInfo() {
    return Row(
      children: [
        _storeLogoItem(),
        const Padding(padding: EdgeInsets.only(left: 10)),
        Expanded(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              _storeInfoItem(),
              _storeAddressItem(),
            ],
          ),
        ),
      ],
    );
  }

  Widget _storeLogoItem() {
    return ClipOval(
      child: CachedNetworkImage(
        placeholder: (context, url) => Image.asset(
          'assets/images/default_goods_cover.webp',
          width: 50,
          height: 50,
        ),
        errorWidget: (context, url, error) => Image.asset(
          'assets/images/default_goods_cover.webp',
          width: 50,
          height: 50,
        ),
        imageUrl: state.storeInfo.value.logo,
        width: 50,
        height: 50,
        fit: BoxFit.cover,
      ),
    );
  }

  Widget _storeAddressItem() {
    return Text(
      state.storeInfo.value.address,
      style: const TextStyle(
        fontSize: 12,
        color: Color(0xb2ffffff),
        overflow: TextOverflow.ellipsis,
      ),
      maxLines: 2,
    );
  }

  Widget _storeInfoItem() {
    return Text.rich(
      TextSpan(
        children: [
          TextSpan(
            text: state.storeInfo.value.name,
            style: const TextStyle(
              fontSize: 18,
              color: Color(0xffFFFFFF),
              overflow: TextOverflow.ellipsis,
            ),
          ),
          const WidgetSpan(child: Padding(padding: EdgeInsets.only(left: 10))),
          TextSpan(
            text: "粉丝数  ${state.storeInfo.value.fansNum}",
            style: const TextStyle(
              fontSize: 12,
              color: Color(0xffFFFFFF),
              overflow: TextOverflow.ellipsis,
            ),
          ),
        ],
      ),
      maxLines: 1,
      overflow: TextOverflow.ellipsis,
    );
  }

  Widget _storeExpiryTime() {
    return Container(
      margin: const EdgeInsets.fromLTRB(0, 15, 0, 10),
      padding: const EdgeInsets.symmetric(vertical: 0, horizontal: 15),
      decoration: const BoxDecoration(
        image: DecorationImage(
          image: AssetImage("assets/images/bg_store_expiry_time.webp"),
          fit: BoxFit.cover,
        ),
        borderRadius: BorderRadius.all(Radius.circular(4)),
      ),
      height: 44,
      child: Row(
        children: [
          const Text(
            "数字化商家",
            style: TextStyle(
              fontSize: 16,
              color: Color(0xffffffff),
              overflow: TextOverflow.ellipsis,
            ),
          ),
          const Padding(padding: EdgeInsets.only(left: 10)),
          Text(
            "${logic.getExpirationTime(state.storeInfo.value.trainExpirationTime)}到期",
            style: const TextStyle(
              fontSize: 14,
              color: Color(0x99ffffff),
              overflow: TextOverflow.ellipsis,
            ),
          ),
          const Spacer(),
          const Text(
            "前往续费 >",
            style: TextStyle(
              fontSize: 12,
              color: Color(0xffffffff),
              overflow: TextOverflow.ellipsis,
            ),
          )
        ],
      ),
    );
  }

  _storeGoodsManageTitle() {
    return const Text(
      "商品管理",
      style: TextStyle(fontWeight: FontWeight.bold),
    );
  }

  _storeGoodsManageWidget() {
    return Container(
      margin: const EdgeInsets.fromLTRB(10, 0, 10, 10),
      padding: const EdgeInsets.fromLTRB(10, 10, 10, 0),
      decoration: const BoxDecoration(
        //背景
        color: Colors.white,
        //设置四周圆角 角度
        borderRadius: BorderRadius.all(Radius.circular(10)),
      ),
      // height: 280,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          _storeGoodsManageTitle(),
          const Padding(padding: EdgeInsets.only(top: 15)),
          GridView.count(
            shrinkWrap: true,
            physics: const NeverScrollableScrollPhysics(),
            padding: const EdgeInsets.fromLTRB(0, 5, 0, 0),
            // crossAxisSpacing: 10.0,
            //纵轴间距
            // mainAxisSpacing: 10.w,
            //横轴元素个数
            crossAxisCount: 4,
            childAspectRatio: 5 / 4,
            children: <Widget>[
              _goodsManageItem(),
              _goodsManageItem(),
              _goodsManageItem(),
              _goodsManageItem(),
              _goodsManageItem(),
              _goodsManageItem(),
            ],
          )
        ],
      ),
    );
  }

  Widget _goodsManageItem() {
    return Column(
      children: [
        Image.asset(
          "assets/images/logo.png",
          width: 36,
          height: 36,
          fit: BoxFit.cover,
        ),
        const Padding(padding: EdgeInsets.only(top: 5)),
        const Text(
          "秒杀商品",
          style: TextStyle(
            fontSize: 12,
            color: Color(0xff333333),
          ),
        )
      ],
    );
  }

  _storeOtherManageWidget() {
    return Container(
      margin: const EdgeInsets.fromLTRB(10, 0, 10, 10),
      padding: const EdgeInsets.fromLTRB(10, 10, 10, 0),
      decoration: const BoxDecoration(
        //背景
        color: Colors.white,
        //设置四周圆角 角度
        borderRadius: BorderRadius.all(Radius.circular(10)),
      ),
      height: 200,
    );
  }
}
