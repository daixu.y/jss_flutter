import 'package:get/get.dart';
import 'package:jss_flutter/bean/store_info_entity.dart';
import 'package:jss_flutter/bean/store_wallet_entity.dart';
import 'package:jss_flutter/http/net/http_client.dart';
import 'package:jss_flutter/http/net/http_response.dart';
import 'package:jss_flutter/utils/date_utils.dart';
import 'package:jss_flutter/utils/log_utils.dart';

import '../../app/config/route_config.dart';
import 'state.dart';

class StoreLogic extends GetxController {
  final StoreState state = StoreState();
  final HttpClient dio = Get.find<HttpClient>();

  /// 可 async 拉取数据
  /// 可触发展示交互组件
  /// onInit 之后
  @override
  void onReady() {
    super.onReady();
    getStoreInfo();
    getStoreWallet();
  }

  String getExpirationTime(int trainExpirationTime) {
    return DateFormatUtils.formatDate("yyyy.MM.dd", trainExpirationTime * 1000);
  }

  void getStoreWallet() async {
    // shop/shop_wallet
    HttpResponse appResponse = await dio.get("shop/shop_wallet");
    var response = StoreWalletEntity.fromJson(appResponse.data);

    if (appResponse.ok) {
      if (null != response.data) {
        state.storeWallet.value = response.data!;
      }
    } else {
      LogUtils.e("==fail==${appResponse.error}");
    }
  }

  void getStoreInfo() async {
    // shop/info
    HttpResponse appResponse = await dio.get("shop/info");
    var response = StoreInfoEntity.fromJson(appResponse.data);

    if (appResponse.ok) {
      if (null != response.data) {
        state.storeInfo.value = response.data!;
        state.loadState.value = 1;
      } else {
        state.loadState.value = 2;
      }
    } else {
      LogUtils.e("==fail==${appResponse.error}");
      state.loadState.value = 3;
    }
  }

  void jumpOrderList(int type) =>
      Get.toNamed(RouteConfig.storeOrder, arguments: type);

  void jump2Revenue() =>  Get.toNamed(RouteConfig.storeRevenue);

  void jumpAfterList() => Get.toNamed(RouteConfig.storeOrder, arguments: 0);
}
