import 'package:get/get.dart';

import 'logic.dart';

class StoreBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => StoreLogic());
  }
}
