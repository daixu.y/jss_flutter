import 'package:get/get.dart';
import 'package:jss_flutter/bean/store_info_entity.dart';
import 'package:jss_flutter/bean/store_wallet_entity.dart';

class StoreState {
  var msg = "";
  var storeInfo = StoreInfoData().obs;
  var storeWallet = StoreWalletData().obs;

  /// 0 未加载 1 加载成功 2 加载失败
  final loadState = 0.obs;

  StoreState() {
    msg = "store首页";
  }
}
