import 'package:get/get.dart';
import 'package:jss_flutter/bean/withdraw_record_entity.dart';
import 'package:jss_flutter/http/net/http_client.dart';
import 'package:jss_flutter/http/net/http_response.dart';
import 'package:jss_flutter/utils/log_utils.dart';
import 'package:jss_flutter/widgets/smart_refresher.dart';

import 'state.dart';

class WithdrawRecordLogic extends GetxController {
  final WithdrawRecordState state = WithdrawRecordState();
  final HttpClient dio = Get.find<HttpClient>();

  final RefreshController refreshController = RefreshController(
    initialRefresh: true,
  );

  /// 可 async 拉取数据
  /// 可触发展示交互组件
  /// onInit 之后
  @override
  void onReady() {
    super.onReady();
    getRecordList(0);
  }

  void getRecordList(int page, {bool isRefresh = false}) async {
    var data = {"status": 0, "type": 6, "page": page};
    // //shop_withdraw/get_list

    if (isRefresh && state.loadState.value == 3) {
      refreshController.resetNoData();
    }

    HttpResponse appResponse =
    await dio.get("shop_withdraw/get_list", queryParameters: data);
    var response = WithdrawRecordEntity.fromJson(appResponse.data);

    if (appResponse.ok) {
      ///加载状态  1加载成功 2下拉加载数据为空 3上拉加载数据为空 4加载失败
      if (null != response.data) {
        if (state.page.value == 1) {
          state.items.clear();
          state.loadState.value = response.data?.isNotEmpty == true ? 1 : 2;
          refreshController.refreshCompleted();
        } else {
          state.loadState.value = response.data?.isNotEmpty == true ? 1 : 3;
          if (response.data?.isNotEmpty == true) {
            state.loadState.value = 1;
            refreshController.loadComplete();
          } else {
            state.loadState.value == 3;
            refreshController.loadNoData();
          }
        }
        state.page.value++;
        state.items.addAll(response.data!);
      } else {
        if (state.page.value == 1) {
          state.items.clear();
          state.loadState.value = 2;
          refreshController.refreshCompleted();
        } else {
          state.loadState.value = 3;
          refreshController.loadNoData();
        }
      }
    } else {
      LogUtils.e("==fail==${appResponse.error}");
      state.loadState.value = 4;

      if (state.page.value == 1) {
        refreshController.refreshCompleted();
      } else {
        refreshController.loadComplete();
      }
    }
  }
}
