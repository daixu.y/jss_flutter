import 'package:get/get.dart';

import 'logic.dart';

class WithdrawRecordBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => WithdrawRecordLogic());
  }
}
