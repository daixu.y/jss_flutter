import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:jss_flutter/bean/withdraw_record_entity.dart';
import 'package:jss_flutter/utils/date_utils.dart';
import 'package:jss_flutter/widgets/smart_refresher.dart';
import 'package:jss_flutter/widgets/smart_widget.dart';

import 'logic.dart';

///商家提现记录
class WithdrawRecordPage extends StatelessWidget {
  final logic = Get.find<WithdrawRecordLogic>();
  final state = Get.find<WithdrawRecordLogic>().state;

  WithdrawRecordPage({Key? key}) : super(key: key);

  ///加载状态  1加载成功 2下拉加载数据为空 3上拉加载数据为空 4加载失败
  void _onRefresh() async {
    state.page.value = 1;
    logic.getRecordList(state.page.value);
  }

  ///加载状态  1加载成功 2下拉加载数据为空 3上拉加载数据为空 4加载失败
  void _onLoading() async {
    logic.getRecordList(state.page.value);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        iconTheme: const IconThemeData(
          color: Colors.black, //修改颜色
        ),
        titleTextStyle: const TextStyle(color: Colors.black, fontSize: 18),
        systemOverlayStyle: SystemUiOverlayStyle.dark,
        backgroundColor: Colors.white,
        centerTitle: true,
        title: const Text("提现记录"),
        elevation: 0,
      ),
      body: SizedBox(
        width: double.infinity,
        height: double.infinity,
        child: Stack(
          alignment: Alignment.center,
          children: [
            // 未加载前显示的动画，加载之后需要隐藏
            Obx(
              () => loadingWidget(state.loadState.value == 0),
            ),
            Obx(
              () => _recordWidget(
                state.loadState.value == 1 || state.loadState.value == 3,
              ),
            ),
            Obx(
              () => loadEmptyWidget(state.loadState.value == 2),
            ),
            Obx(
              () => loadErrorWidget(state.loadState.value == 4),
            ),
          ],
        ),
      ),
      backgroundColor: const Color(0xfff5f5f5),
    );
  }

  Widget _recordWidget(bool visible) {
    return Visibility(
      visible: visible,
      child: SmartRefresher(
        enablePullDown: true,
        enablePullUp: true,
        // header: const WaterDropHeader(),
        header: customHeader(),
        footer: customFooter(),
        controller: logic.refreshController,
        onRefresh: _onRefresh,
        onLoading: _onLoading,
        child: ListView.builder(
          itemCount: state.items.length,
          itemBuilder: (context, index) {
            return _buildItem(state.items[index], index);
          },
        ),
      ),
    );
  }

  Widget _buildItem(WithdrawRecordData item, int index) {
    return Container(
      height: 80,
      margin: const EdgeInsets.fromLTRB(10, 10, 10, 0),
      padding: const EdgeInsets.all(10),
      decoration: const BoxDecoration(
        //背景
        color: Colors.white,
        //设置四周圆角 角度
        borderRadius: BorderRadius.all(Radius.circular(10)),
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          recordItem(item),
          const Padding(
            padding: EdgeInsets.only(top: 10),
          ),
          recordItem2(item),
        ],
      ),
    );
  }

  Widget recordItem(WithdrawRecordData item) {
    var cardNo = item.cardNo;
    if (cardNo.length > 4) {
      cardNo =
          "提现至 ${item.bankName} (**** ${cardNo.substring(cardNo.length - 4)})";
    } else {
      cardNo = "提现至 ${item.bankName} (**** $cardNo)";
    }
    return Row(
      children: [
        Expanded(
          child: Text(
            cardNo,
            style: const TextStyle(
              fontSize: 14,
              color: Color(0xff333333),
            ),
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(left: 5),
          child: Text(
            "+${item.money}",
            style: const TextStyle(
              fontSize: 16,
              color: Color(0xff333333),
            ),
          ),
        ),
      ],
    );
  }

  Widget recordItem2(WithdrawRecordData item) {
    var statusText = "";
    if (item.status == 1) {
      statusText = "提现中";
    } else if (item.status == 2) {
      statusText = "提现成功";
    } else {
      statusText = "提现失败";
    }
    return Row(
      children: [
        Expanded(
          child: Text(
            DateFormatUtils.formatTime(
              "yyyy-MM-dd'T'HH:mm:ss.SSSSSS'Z'",
              "yyyy-MM-dd HH:mm:ss",
              item.time,
            ),
            style: const TextStyle(
              fontSize: 12,
              color: Color(0xff999999),
            ),
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(left: 5),
          child: Text(
            statusText,
            style: TextStyle(
              fontSize: 12,
              color: item.status == 1
                  ? const Color(0xff999999)
                  : item.status == 2
                      ? const Color(0xff6096F7)
                      : const Color(0xffff4954),
            ),
          ),
        ),
      ],
    );
  }
}
