import 'package:get/get.dart';
import 'package:jss_flutter/bean/withdraw_record_entity.dart';

class WithdrawRecordState {
  final items = <WithdrawRecordData>[].obs;

  ///加载状态  0加载中 1加载成功 2下拉加载数据为空 3上拉加载数据为空 4加载失败
  final loadState = 0.obs;
  final page = 1.obs;
  WithdrawRecordState() {
    ///Initialize variables
  }
}
