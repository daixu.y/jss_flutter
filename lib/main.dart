import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_loggy/flutter_loggy.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:loggy/loggy.dart';

import 'app/config/route_config.dart';
import 'http/net/http_client.dart';
import 'http/net/http_config.dart';

void main() async {
  await GetStorage.init();

  HttpConfig dioConfig = HttpConfig(baseUrl: "https://jss.test.zjjushuashua.com/api/");
  // HttpConfig(baseUrl: "https://gank.io/", proxy: "192.168.2.249:8888");
  HttpClient client = HttpClient(dioConfig: dioConfig);
  Get.put<HttpClient>(client);

  Loggy.initLoggy(
      logPrinter: const PrettyDeveloperPrinter()
  );

  runApp(const MyApp());
  // 透明状态栏
  if (Platform.isAndroid) {
    SystemUiOverlayStyle systemUiOverlayStyle = const SystemUiOverlayStyle(statusBarColor: Colors.transparent);
    SystemChrome.setSystemUIOverlayStyle(systemUiOverlayStyle);
  }

}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      initialRoute: RouteConfig.index,
      getPages: RouteConfig.getPages,
      title: "聚刷刷",
    );
  }
}