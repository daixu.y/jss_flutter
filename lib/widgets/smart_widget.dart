import 'package:flutter/cupertino.dart';
import 'package:jss_flutter/widgets/smart_refresher.dart';
import 'package:lottie/lottie.dart';

import 'indicator/custom_indicator.dart';

Widget loadingWidget(bool visible) {
  return Visibility(
    visible: visible,
    child: SizedBox(
      width: 200,
      height: 200,
      child: Lottie.asset(
        "assets/lottie/page_loading_anim.json",
        width: 200,
        height: 200,
        animate: true,
      ),
    ),
  );
}

Widget loadEmptyWidget(bool visible, {String empty = "没有更多内容啦~"}) {
  return Visibility(
    visible: visible,
    child: Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Lottie.asset(
          "assets/lottie/refresh_empty_page.json",
          width: 200,
          animate: true,
          repeat: false,
        ),
        Text(
          empty,
          style: const TextStyle(
            fontSize: 14,
            color: Color(0xFFB8C0D4),
          ),
        )
      ],
    ),
  );
}

Widget loadErrorWidget(bool visible) {
  return Visibility(
    visible: visible,
    child: Lottie.asset(
      "assets/lottie/refresh_error.json",
      width: 200,
      animate: true,
    ),
  );
}

Widget customHeader() {
  return CustomHeader(
    builder: (BuildContext context, RefreshStatus? mode) {
      Widget header;
      if (mode == RefreshStatus.idle) {
        ///下拉时显示
        header = const Text(
          "上拉刷新",
          style: TextStyle(color: Color(0xFFB8C0D4), fontSize: 14),
        );
      } else if (mode == RefreshStatus.refreshing) {
        ///加载中
        header = Lottie.asset("assets/lottie/refresh_head_loading.json",
            width: 100, animate: true);
      } else if (mode == RefreshStatus.failed) {
        ///加载失败
        header = const Text(
          "刷新失败!",
          style: TextStyle(color: Color(0xFFB8C0D4), fontSize: 14),
        );
      } else if (mode == RefreshStatus.completed) {
        ///加载成功
        header = const Text(
          "刷新成功",
          style: TextStyle(color: Color(0xFFB8C0D4), fontSize: 14),
        );
      } else {
        ///超过二层
        header = const Text(
          "释放刷新",
          style: TextStyle(color: Color(0xFFB8C0D4), fontSize: 14),
        );
      }
      return SizedBox(
        height: 64,
        child: Center(child: header),
      );
    },
  );
}

Widget customFooter() {
  return CustomFooter(
    builder: (BuildContext context, LoadStatus? mode) {
      Widget body;
      if (mode == LoadStatus.idle) {
        body = const Text("上拉加载");
      } else if (mode == LoadStatus.loading) {
        body = Lottie.asset("assets/lottie/refresh_footer.json",
            width: 200, animate: true);
      } else if (mode == LoadStatus.failed) {
        body = const Text("加载失败！点击重试！");
      } else if (mode == LoadStatus.canLoading) {
        body = const Text("松手,加载更多!");
      } else {
        body = const Text("没有更多数据了!");
      }
      return SizedBox(
        height: 55,
        child: Center(child: body),
      );
    },
  );
}
