
class HttpException implements Exception {
  final String? _msg;

  String get msg => _msg ?? runtimeType.toString();

  final int? _code;

  int get code => _code ?? -1;

  HttpException([this._msg, this._code]);

  @override
  String toString() {
    return "code:$code--msg=$msg";
  }
}

/// 客户端请求错误
class BadRequestException extends HttpException {
  BadRequestException({String? msg, int? code}) : super(msg, code);
}

/// 服务端响应错误
class BadServiceException extends HttpException {
  BadServiceException({String? msg, int? code}) : super(msg, code);
}

class UnknownException extends HttpException {
  UnknownException([String? msg]) : super(msg);
}

class CancelException extends HttpException {
  CancelException([String? msg]) : super(msg);
}

class NetworkException extends HttpException {
  NetworkException({String? msg, int? code}) : super(msg, code);
}

/// 401
class UnauthorisedException extends HttpException {
  UnauthorisedException({String? msg, int? code = 401}) : super(msg);
}

class BadResponseException extends HttpException {
  dynamic? data;

  BadResponseException([this.data]) : super();
}
