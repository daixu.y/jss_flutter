import 'package:dio/dio.dart';
import 'package:jss_flutter/http/net/http_response.dart';

/// Response 解析
abstract class HttpTransformer {
  HttpResponse parse(Response response);
}
