import 'dart:convert';
import 'package:jss_flutter/generated/json/base/json_field.dart';
import 'package:jss_flutter/generated/json/barter_goods_entity.g.dart';

@JsonSerializable()
class BarterGoodsEntity {

	int? code;
	List<BarterGoodsData>? data;
	String? msg;
	int? status;
  
  BarterGoodsEntity();

  factory BarterGoodsEntity.fromJson(Map<String, dynamic> json) => $BarterGoodsEntityFromJson(json);

  Map<String, dynamic> toJson() => $BarterGoodsEntityToJson(this);

  @override
  String toString() {
    return jsonEncode(this);
  }
}

@JsonSerializable()
class BarterGoodsData {

	int? id;
	@JSONField(name: "activity_id")
	int? activityId;
	@JSONField(name: "goods_id")
	int? goodsId;
	@JSONField(name: "is_afhalen")
	int? isAfhalen;
	String? price;
	@JSONField(name: "goods_sales")
	int? goodsSales;
	@JSONField(name: "show_type")
	int? showType;
	@JSONField(name: "goods_name")
	String? goodsName;
	@JSONField(name: "goods_image")
	String goodsImage = "";
	@JSONField(name: "shop_name")
	String? shopName;
	@JSONField(name: "shop_logo")
	String? shopLogo;
	@JSONField(name: "shop_lng")
	double? shopLng;
	@JSONField(name: "shop_lat")
	double? shopLat;
	double? distance;
	String? inventory;
	@JSONField(name: "total_inventory")
	String? totalInventory;
  
  BarterGoodsData();

  factory BarterGoodsData.fromJson(Map<String, dynamic> json) => $BarterGoodsDataFromJson(json);

  Map<String, dynamic> toJson() => $BarterGoodsDataToJson(this);

  @override
  String toString() {
    return jsonEncode(this);
  }
}