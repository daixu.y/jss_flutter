import 'dart:convert';
import 'package:jss_flutter/generated/json/base/json_field.dart';
import 'package:jss_flutter/generated/json/bill_list_entity.g.dart';

@JsonSerializable()
class BillListEntity {

	int? code;
	List<BillListData>? data;
	String? msg;
	int? status;
  
  BillListEntity();

  factory BillListEntity.fromJson(Map<String, dynamic> json) => $BillListEntityFromJson(json);

  Map<String, dynamic> toJson() => $BillListEntityToJson(this);

  @override
  String toString() {
    return jsonEncode(this);
  }
}

@JsonSerializable()
class BillListData {

	int? id;
	String? money;
	@JSONField(name: "type_text")
	String typeText = "";
	@JSONField(name: "surplus_money")
	String? surplusMoney;
	@JSONField(name: "old_money")
	String? oldMoney;
	@JSONField(name: "new_money")
	String? newMoney;
	String remark = "";
	@JSONField(name: "created_at")
	String? createdAt;
  
  BillListData();

  factory BillListData.fromJson(Map<String, dynamic> json) => $BillListDataFromJson(json);

  Map<String, dynamic> toJson() => $BillListDataToJson(this);

  @override
  String toString() {
    return jsonEncode(this);
  }
}