import 'dart:convert';
import 'package:jss_flutter/generated/json/base/json_field.dart';
import 'package:jss_flutter/generated/json/login_entity.g.dart';

@JsonSerializable()
class LoginEntity {

	int? code;
	LoginData? data;
	String? msg;
	int? status;
  
  LoginEntity();

  factory LoginEntity.fromJson(Map<String, dynamic> json) => $LoginEntityFromJson(json);

  Map<String, dynamic> toJson() => $LoginEntityToJson(this);

  @override
  String toString() {
    return jsonEncode(this);
  }
}

@JsonSerializable()
class LoginData {

	String? token;
  
  LoginData();

  factory LoginData.fromJson(Map<String, dynamic> json) => $LoginDataFromJson(json);

  Map<String, dynamic> toJson() => $LoginDataToJson(this);

  @override
  String toString() {
    return jsonEncode(this);
  }
}