import 'dart:convert';
import 'package:jss_flutter/generated/json/base/json_field.dart';
import 'package:jss_flutter/generated/json/order_list_entity.g.dart';

@JsonSerializable()
class OrderListEntity {

	int? code;
	List<OrderListData>? data;
	String? msg;
	int? status;
  
  OrderListEntity();

  factory OrderListEntity.fromJson(Map<String, dynamic> json) => $OrderListEntityFromJson(json);

  Map<String, dynamic> toJson() => $OrderListEntityToJson(this);

  @override
  String toString() {
    return jsonEncode(this);
  }
}

@JsonSerializable()
class OrderListData {

	int? id;
	@JSONField(name: "order_no")
	String orderNo = "";
	String? time;
	int? status;
	@JSONField(name: "status_text")
	String statusText = "";
	@JSONField(name: "cut_end")
	int? cutEnd;
	@JSONField(name: "after_sale_status")
	int? afterSaleStatus;
	@JSONField(name: "comment_status")
	int? commentStatus;
	int? type;
	String unpaid = "";
	@JSONField(name: "courier_no")
	String? courierNo;
	@JSONField(name: "courier_com")
	String? courierCom;
	@JSONField(name: "delivery_way")
	int? deliveryWay;
	@JSONField(name: "shipment_time")
	int? shipmentTime;
	@JSONField(name: "order_type")
	int? orderType;
	OrderListDataMember? member;
	@JSONField(name: "goods_list")
	List<OrderListDataGoodsList>? goodsList;
  
  OrderListData();

  factory OrderListData.fromJson(Map<String, dynamic> json) => $OrderListDataFromJson(json);

  Map<String, dynamic> toJson() => $OrderListDataToJson(this);

  @override
  String toString() {
    return jsonEncode(this);
  }
}

@JsonSerializable()
class OrderListDataMember {

	String nickname = "";
	String headimgurl = "";
	@JSONField(name: "status_text")
	String? statusText;
  
  OrderListDataMember();

  factory OrderListDataMember.fromJson(Map<String, dynamic> json) => $OrderListDataMemberFromJson(json);

  Map<String, dynamic> toJson() => $OrderListDataMemberToJson(this);

  @override
  String toString() {
    return jsonEncode(this);
  }
}

@JsonSerializable()
class OrderListDataGoodsList {

	@JSONField(name: "goods_name")
	String goodsName = "";
	@JSONField(name: "goods_image")
	String goodsImage = "";
	int num = 0;
	@JSONField(name: "sku_name")
	String skuName = "";
	String price = "";
	@JSONField(name: "goods_id")
	int goodsId = 0;
  
  OrderListDataGoodsList();

  factory OrderListDataGoodsList.fromJson(Map<String, dynamic> json) => $OrderListDataGoodsListFromJson(json);

  Map<String, dynamic> toJson() => $OrderListDataGoodsListToJson(this);

  @override
  String toString() {
    return jsonEncode(this);
  }
}