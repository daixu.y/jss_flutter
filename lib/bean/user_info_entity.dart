import 'dart:convert';
import 'package:jss_flutter/generated/json/base/json_field.dart';
import 'package:jss_flutter/generated/json/user_info_entity.g.dart';

@JsonSerializable()
class UserInfoEntity {

	int? code;
	@JSONField(name: "error_type")
	int? errorType;
	UserInfoData? data;
	String? msg;
	String? mess;
	int? count;
	int? status;
  
  UserInfoEntity();

  factory UserInfoEntity.fromJson(Map<String, dynamic> json) => $UserInfoEntityFromJson(json);

  Map<String, dynamic> toJson() => $UserInfoEntityToJson(this);

  @override
  String toString() {
    return jsonEncode(this);
  }
}

@JsonSerializable()
class UserInfoData {

	@JSONField(name: "member_id")
	int? memberId;
	String? nickname;
	String? headimgurl;
	String? phone;
	@JSONField(name: "creator_status")
	int? creatorStatus;
	@JSONField(name: "cut_coupon_num")
	int? cutCouponNum;
	@JSONField(name: "cut_coupon_past_num")
	int? cutCouponPastNum;
	@JSONField(name: "jg_name")
	String? jgName;
	int? credit;
	@JSONField(name: "cut_level")
	int? cutLevel;
	String? money;
	@JSONField(name: "cut_earnings")
	String? cutEarnings;
	@JSONField(name: "barter_money")
	String? barterMoney;
	@JSONField(name: "ju_money")
	String? juMoney;
	@JSONField(name: "voucher_money")
	String? voucherMoney;
	@JSONField(name: "share_money")
	String? shareMoney;
	@JSONField(name: "share_money_total")
	String? shareMoneyTotal;
	@JSONField(name: "share_barter_total")
	String? shareBarterTotal;
	@JSONField(name: "share_cut_total")
	int? shareCutTotal;
	@JSONField(name: "share_cut_coupon_total")
	int? shareCutCouponTotal;
	@JSONField(name: "yesterday_income")
	int? yesterdayIncome;
	@JSONField(name: "shop_id")
	int? shopId;
	int? sex;
	@JSONField(name: "isset_pay_password")
	int? issetPayPassword;
	String? birthday;
	String? signature;
	int? master;
	@JSONField(name: "invite_code")
	String? inviteCode;
	int? supermarket;
	@JSONField(name: "plan_num")
	int? planNum;
	@JSONField(name: "is_shop_share")
	int? isShopShare;
  
  UserInfoData();

  factory UserInfoData.fromJson(Map<String, dynamic> json) => $UserInfoDataFromJson(json);

  Map<String, dynamic> toJson() => $UserInfoDataToJson(this);

  @override
  String toString() {
    return jsonEncode(this);
  }
}