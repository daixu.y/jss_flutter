import 'dart:convert';
import 'package:jss_flutter/generated/json/base/json_field.dart';
import 'package:jss_flutter/generated/json/goods_detail_entity.g.dart';

@JsonSerializable()
class GoodsDetailEntity {

	int? code;
	GoodsDetailData? data;
	String? msg;
	int? status;
  
  GoodsDetailEntity();

  factory GoodsDetailEntity.fromJson(Map<String, dynamic> json) => $GoodsDetailEntityFromJson(json);

  Map<String, dynamic> toJson() => $GoodsDetailEntityToJson(this);

  @override
  String toString() {
    return jsonEncode(this);
  }
}

@JsonSerializable()
class GoodsDetailData {

	int? id;
	@JSONField(name: "category_id_one")
	int? categoryIdOne;
	@JSONField(name: "category_id_two")
	int? categoryIdTwo;
	@JSONField(name: "category_id_three")
	int? categoryIdThree;
	@JSONField(name: "shop_category_id_one")
	int? shopCategoryIdOne;
	@JSONField(name: "shop_category_id_two")
	int? shopCategoryIdTwo;
	@JSONField(name: "shop_category_id_three")
	int? shopCategoryIdThree;
	String name = "";
	String? image;
	String? video;
	String? desc;
	@JSONField(name: "show_type")
	int? showType;
	String price = "";
	@JSONField(name: "market_price")
	String marketPrice = "";
	int? status;
	@JSONField(name: "audit_status")
	int? auditStatus;
	@JSONField(name: "audit_remark")
	String? auditRemark;
	int? sales;
	int? inventory;
	int? type;
	@JSONField(name: "share_ratio")
	String? shareRatio;
	int? sort;
	@JSONField(name: "city_id")
	int? cityId;
	@JSONField(name: "shop_id")
	int? shopId;
	@JSONField(name: "is_sync")
	int? isSync;
	@JSONField(name: "freight_tmp_id")
	int? freightTmpId;
	@JSONField(name: "is_afhalen")
	int? isAfhalen;
	@JSONField(name: "created_at")
	String? createdAt;
	@JSONField(name: "updated_at")
	String? updatedAt;
	@JSONField(name: "audit_status_text")
	String? auditStatusText;
	@JSONField(name: "status_text")
	String? statusText;
	List<GoodsDetailDataImages>? images;
	List<GoodsDetailDataSkus>? skus;
	@JSONField(name: "shop_info")
	GoodsDetailDataShopInfo? shopInfo;
	@JSONField(name: "is_collect")
	int? isCollect;
	@JSONField(name: "plan_list")
	List<GoodsDetailDataPlanList>? planList;
	int? distance;
	@JSONField(name: "category_name_one")
	String? categoryNameOne;
	@JSONField(name: "category_name_two")
	String? categoryNameTwo;
	@JSONField(name: "category_name_three")
	String? categoryNameThree;
	@JSONField(name: "share_show")
	int? shareShow;
  
  GoodsDetailData();

  factory GoodsDetailData.fromJson(Map<String, dynamic> json) => $GoodsDetailDataFromJson(json);

  Map<String, dynamic> toJson() => $GoodsDetailDataToJson(this);

  @override
  String toString() {
    return jsonEncode(this);
  }
}

@JsonSerializable()
class GoodsDetailDataImages {

	String image = "";
	int? type;
  
  GoodsDetailDataImages();

  factory GoodsDetailDataImages.fromJson(Map<String, dynamic> json) => $GoodsDetailDataImagesFromJson(json);

  Map<String, dynamic> toJson() => $GoodsDetailDataImagesToJson(this);

  @override
  String toString() {
    return jsonEncode(this);
  }
}

@JsonSerializable()
class GoodsDetailDataSkus {

	int? id;
	String? price;
	String? image;
	String? name;
	int? inventory;
	@JSONField(name: "is_permanent_validity")
	int? isPermanentValidity;
	@JSONField(name: "end_time")
	String? endTime;
	@JSONField(name: "cut_total_commission")
	String? cutTotalCommission;
	@JSONField(name: "attr_value_ids")
	String? attrValueIds;
	@JSONField(name: "cut_time")
	int? cutTime;
	@JSONField(name: "cut_people")
	int? cutPeople;
	@JSONField(name: "give_cut_coupon_num")
	int? giveCutCouponNum;
  
  GoodsDetailDataSkus();

  factory GoodsDetailDataSkus.fromJson(Map<String, dynamic> json) => $GoodsDetailDataSkusFromJson(json);

  Map<String, dynamic> toJson() => $GoodsDetailDataSkusToJson(this);

  @override
  String toString() {
    return jsonEncode(this);
  }
}

@JsonSerializable()
class GoodsDetailDataShopInfo {

	String? logo;
	double? lat;
	double? lng;
	@JSONField(name: "member_id")
	int? memberId;
	String? name;
	String? address;
	@JSONField(name: "region_info")
	String? regionInfo;
	String? phone;
	@JSONField(name: "is_live")
	int? isLive;
	@JSONField(name: "audit_status_text")
	String? auditStatusText;
	@JSONField(name: "earnest_status_text")
	String? earnestStatusText;
	@JSONField(name: "status_text")
	String? statusText;
  
  GoodsDetailDataShopInfo();

  factory GoodsDetailDataShopInfo.fromJson(Map<String, dynamic> json) => $GoodsDetailDataShopInfoFromJson(json);

  Map<String, dynamic> toJson() => $GoodsDetailDataShopInfoToJson(this);

  @override
  String toString() {
    return jsonEncode(this);
  }
}

@JsonSerializable()
class GoodsDetailDataPlanList {

	String? id;
	@JSONField(name: "shop_id")
	String? shopId;
	int? online;
	int? type;
	String title = "";
	String amount = "";
	@JSONField(name: "limit_amount")
	int? limitAmount;
	int discount = 0;
	@JSONField(name: "goods_ids")
	String? goodsIds;
	int? number;
	@JSONField(name: "get_number")
	int? getNumber;
	@JSONField(name: "expire_number")
	int? expireNumber;
	@JSONField(name: "use_number")
	int? useNumber;
	int? status;
	@JSONField(name: "is_new")
	int? isNew;
	@JSONField(name: "start_time")
	int? startTime;
	@JSONField(name: "end_time")
	int? endTime;
	@JSONField(name: "created_at")
	String? createdAt;
	@JSONField(name: "updated_at")
	String? updatedAt;
	@JSONField(name: "is_get")
	int? isGet;
  
  GoodsDetailDataPlanList();

  factory GoodsDetailDataPlanList.fromJson(Map<String, dynamic> json) => $GoodsDetailDataPlanListFromJson(json);

  Map<String, dynamic> toJson() => $GoodsDetailDataPlanListToJson(this);

  @override
  String toString() {
    return jsonEncode(this);
  }
}