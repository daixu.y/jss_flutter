import 'dart:convert';
import 'package:jss_flutter/generated/json/base/json_field.dart';
import 'package:jss_flutter/generated/json/store_wallet_entity.g.dart';

@JsonSerializable()
class StoreWalletEntity {

	int? code;
	StoreWalletData? data;
	String? msg;
	int? status;
  
  StoreWalletEntity();

  factory StoreWalletEntity.fromJson(Map<String, dynamic> json) => $StoreWalletEntityFromJson(json);

  Map<String, dynamic> toJson() => $StoreWalletEntityToJson(this);

  @override
  String toString() {
    return jsonEncode(this);
  }
}

@JsonSerializable()
class StoreWalletData {

	@JSONField(name: "total_money")
	String totalMoney = "";
	@JSONField(name: "shop_money")
	String shopMoney = "";
	@JSONField(name: "shop_account_frozen")
	String shopAccountFrozen = "";
	@JSONField(name: "shop_no_settlement")
	String shopNoSettlement = "";
	@JSONField(name: "shop_money_withdraw_total")
	String shopMoneyWithdrawTotal = "";
	@JSONField(name: "barter_money")
	String barterMoney = "";
	@JSONField(name: "yesterday_money")
	String yesterdayMoney = "";
  
  StoreWalletData();

  factory StoreWalletData.fromJson(Map<String, dynamic> json) => $StoreWalletDataFromJson(json);

  Map<String, dynamic> toJson() => $StoreWalletDataToJson(this);

  @override
  String toString() {
    return jsonEncode(this);
  }
}