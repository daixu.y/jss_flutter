import 'dart:convert';
import 'package:jss_flutter/generated/json/base/json_field.dart';
import 'package:jss_flutter/generated/json/store_info_entity.g.dart';

@JsonSerializable()
class StoreInfoEntity {

	int? code;
	StoreInfoData? data;
	String? msg;
	int? status;
  
  StoreInfoEntity();

  factory StoreInfoEntity.fromJson(Map<String, dynamic> json) => $StoreInfoEntityFromJson(json);

  Map<String, dynamic> toJson() => $StoreInfoEntityToJson(this);

  @override
  String toString() {
    return jsonEncode(this);
  }
}

@JsonSerializable()
class StoreInfoData {

	int? id;
	String name = "";
	int? status;
	@JSONField(name: "earnest_status")
	int? earnestStatus;
	String logo = "";
	String? image;
	List<String>? images;
	@JSONField(name: "train_status")
	int? trainStatus;
	@JSONField(name: "train_expiration_time")
	int trainExpirationTime = 0;
	double? lat;
	double? lng;
	String? phone;
	@JSONField(name: "business_hours")
	String? businessHours;
	@JSONField(name: "pro_id")
	int? proId;
	@JSONField(name: "area_id")
	int? areaId;
	@JSONField(name: "goods_mode")
	int? goodsMode;
	@JSONField(name: "city_id")
	int? cityId;
	@JSONField(name: "street_id")
	int? streetId;
	@JSONField(name: "trade_id")
	int? tradeId;
	@JSONField(name: "trade_name")
	String? tradeName;
	@JSONField(name: "region_info")
	String? regionInfo;
	String address = "";
	String? desc;
	@JSONField(name: "audit_status_text")
	String? auditStatusText;
	@JSONField(name: "earnest_status_text")
	String? earnestStatusText;
	@JSONField(name: "status_text")
	String? statusText;
	@JSONField(name: "fans_num")
	String? fansNum;
	StoreInfoDataCert? cert;
	String? earnest;
  
  StoreInfoData();

  factory StoreInfoData.fromJson(Map<String, dynamic> json) => $StoreInfoDataFromJson(json);

  Map<String, dynamic> toJson() => $StoreInfoDataToJson(this);

  @override
  String toString() {
    return jsonEncode(this);
  }
}

@JsonSerializable()
class StoreInfoDataCert {

	int? id;
	@JSONField(name: "com_name")
	String? comName;
	String? nature;
	@JSONField(name: "com_shengshiqu")
	String? comShengshiqu;
	@JSONField(name: "com_address")
	String? comAddress;
	@JSONField(name: "fixed_phone")
	String? fixedPhone;
	@JSONField(name: "com_email")
	String? comEmail;
	String? zczj;
	String? tyxydm;
	@JSONField(name: "faren_name")
	String? farenName;
	@JSONField(name: "zzstart_time")
	int? zzstartTime;
	@JSONField(name: "zzend_time")
	int? zzendTime;
	String? jyfw;
	String? jiancheng;
	@JSONField(name: "sfz_num")
	String? sfzNum;
	@JSONField(name: "sfzz_pic")
	String? sfzzPic;
	@JSONField(name: "sfzb_pic")
	String? sfzbPic;
	@JSONField(name: "frsfz_pic")
	String? frsfzPic;
	String? zhizhao;
	@JSONField(name: "apply_type")
	int? applyType;
	@JSONField(name: "apply_time")
	int? applyTime;
	@JSONField(name: "shop_id")
	int? shopId;
	@JSONField(name: "sfz_limit")
	int? sfzLimit;
	@JSONField(name: "sfz_time")
	String? sfzTime;
	@JSONField(name: "is_szhy")
	int? isSzhy;
	String? khxkz;
	@JSONField(name: "zhizhao_num")
	String? zhizhaoNum;
	String? zzjgdm;
	String? nsrsbm;
	@JSONField(name: "zzjgdm_pic")
	String? zzjgdmPic;
	@JSONField(name: "nsrsbm_pic")
	String? nsrsbmPic;
	@JSONField(name: "zzjgdm_time")
	String? zzjgdmTime;
	@JSONField(name: "created_at")
	String? createdAt;
	@JSONField(name: "updated_at")
	String? updatedAt;
	@JSONField(name: "deleted_at")
	dynamic deletedAt;
	@JSONField(name: "type_text")
	String? typeText;
	@JSONField(name: "sfz_limit_text")
	String? sfzLimitText;
	@JSONField(name: "is_szhy_text")
	String? isSzhyText;
  
  StoreInfoDataCert();

  factory StoreInfoDataCert.fromJson(Map<String, dynamic> json) => $StoreInfoDataCertFromJson(json);

  Map<String, dynamic> toJson() => $StoreInfoDataCertToJson(this);

  @override
  String toString() {
    return jsonEncode(this);
  }
}