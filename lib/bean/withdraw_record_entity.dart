import 'dart:convert';
import 'package:jss_flutter/generated/json/base/json_field.dart';
import 'package:jss_flutter/generated/json/withdraw_record_entity.g.dart';

@JsonSerializable()
class WithdrawRecordEntity {

	int? code;
	List<WithdrawRecordData>? data;
	String? msg;
	int? status;
  
  WithdrawRecordEntity();

  factory WithdrawRecordEntity.fromJson(Map<String, dynamic> json) => $WithdrawRecordEntityFromJson(json);

  Map<String, dynamic> toJson() => $WithdrawRecordEntityToJson(this);

  @override
  String toString() {
    return jsonEncode(this);
  }
}

@JsonSerializable()
class WithdrawRecordData {

	String money = "";
	@JSONField(name: "account_money")
	String? accountMoney;
	@JSONField(name: "service_charge")
	String? serviceCharge;
	@JSONField(name: "status_text")
	String? statusText;
	int? status;
	String time = "";
	@JSONField(name: "audit_time")
	String? auditTime;
	int? id;
	String? remark;
	@JSONField(name: "bank_name")
	String bankName = "";
	@JSONField(name: "card_no")
	String cardNo = "";
  
  WithdrawRecordData();

  factory WithdrawRecordData.fromJson(Map<String, dynamic> json) => $WithdrawRecordDataFromJson(json);

  Map<String, dynamic> toJson() => $WithdrawRecordDataToJson(this);

  @override
  String toString() {
    return jsonEncode(this);
  }
}