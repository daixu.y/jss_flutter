import 'dart:convert';
import 'package:jss_flutter/generated/json/base/json_field.dart';
import 'package:jss_flutter/generated/json/goods_response_entity.g.dart';

@JsonSerializable()
class GoodsResponseEntity {

	int? code;
	List<GoodsResponseData>? data;
	String? msg;
	int? status;
  
  GoodsResponseEntity();

  factory GoodsResponseEntity.fromJson(Map<String, dynamic> json) => $GoodsResponseEntityFromJson(json);

  Map<String, dynamic> toJson() => $GoodsResponseEntityToJson(this);

  @override
  String toString() {
    return jsonEncode(this);
  }
}

@JsonSerializable()
class GoodsResponseData {

	int? id;
	String name = "";
	String image = "";
	String? price;
	String video = "";
	@JSONField(name: "show_type")
	int? showType;
	@JSONField(name: "market_price")
	String? marketPrice;
	int? status;
	int? inventory;
	int? sales;
	@JSONField(name: "freight_tmp_id")
	int? freightTmpId;
	int? type;
	@JSONField(name: "is_afhalen")
	int? isAfhalen;
	@JSONField(name: "audit_status_text")
	late String auditStatusText;
	@JSONField(name: "status_text")
	String? statusText;
  
  GoodsResponseData();

  factory GoodsResponseData.fromJson(Map<String, dynamic> json) => $GoodsResponseDataFromJson(json);

  Map<String, dynamic> toJson() => $GoodsResponseDataToJson(this);

  @override
  String toString() {
    return jsonEncode(this);
  }
}