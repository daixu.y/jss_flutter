import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';

Widget circleImageWidget(String imageUrl, double width, double height,
    {String placeholder = "assets/images/default_goods_cover.webp",
    String errorWidget = "assets/images/default_goods_cover.webp"}) {
  return ClipOval(
    child: CachedNetworkImage(
      placeholder: (context, url) => Image.asset(
        placeholder,
        width: width,
        height: height,
      ),
      errorWidget: (context, url, error) => Image.asset(
        errorWidget,
        width: width,
        height: height,
      ),
      imageUrl: imageUrl,
      width: width,
      height: height,
      fit: BoxFit.cover,
    ),
  );
}

Widget roundImageWidget(String imageUrl, double width, double height,
    {String placeholder = "assets/images/default_goods_cover.webp",
    String errorWidget = "assets/images/default_goods_cover.webp",
    double radius = 6}) {
  return ClipRRect(
    borderRadius: BorderRadius.all(Radius.circular(radius)),
    child: CachedNetworkImage(
      placeholder: (context, url) => Image.asset(
        placeholder,
        width: width,
        height: height,
      ),
      errorWidget: (context, url, error) => Image.asset(
        errorWidget,
        width: width,
        height: height,
      ),
      imageUrl: imageUrl,
      width: width,
      height: height,
      fit: BoxFit.cover,
    ),
  );
}
