import 'package:intl/intl.dart';

class DateFormatUtils {
  static String formatDate(String newPattern, int millisecondsSinceEpoch) {
    DateFormat inputFormat = DateFormat(newPattern);
    return inputFormat.format(DateTime.fromMillisecondsSinceEpoch(millisecondsSinceEpoch));
  }

  static String formatTime(String pattern, String newPattern, String inputString) {
    DateFormat inputFormat = DateFormat(pattern);
    DateTime dateTime = inputFormat.parse(inputString);
    DateFormat outputFormat = DateFormat(newPattern);
    String dateInString = outputFormat.format(dateTime);
    return dateInString;
  }
}
