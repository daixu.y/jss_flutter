import 'package:get/get_navigation/src/routes/get_route.dart';
import 'package:jss_flutter/module/goods/detail/binding.dart';
import 'package:jss_flutter/module/goods/detail/view.dart';
import 'package:jss_flutter/module/login/binding.dart';
import 'package:jss_flutter/module/login/view.dart';
import 'package:jss_flutter/module/main/binding.dart';
import 'package:jss_flutter/module/main/view.dart';
import 'package:jss_flutter/module/my/setting/binding.dart';
import 'package:jss_flutter/module/my/setting/view.dart';
import 'package:jss_flutter/module/splash/binding.dart';
import 'package:jss_flutter/module/splash/view.dart';
import 'package:jss_flutter/module/store/bill/detail/binding.dart';
import 'package:jss_flutter/module/store/bill/detail/view.dart';
import 'package:jss_flutter/module/store/binding.dart';
import 'package:jss_flutter/module/store/order/binding.dart';
import 'package:jss_flutter/module/store/order/detail/binding.dart';
import 'package:jss_flutter/module/store/order/detail/view.dart';
import 'package:jss_flutter/module/store/order/view.dart';
import 'package:jss_flutter/module/store/revenue/binding.dart';
import 'package:jss_flutter/module/store/revenue/view.dart';
import 'package:jss_flutter/module/store/view.dart';
import 'package:jss_flutter/module/store/withdraw/record/binding.dart';
import 'package:jss_flutter/module/store/withdraw/record/view.dart';

class RouteConfig {
  ///根页面
  static const String index = "/";

  ///登录页面
  static const String login = "/login";

  ///主页面
  static const String main = "/main";

  ///商品详情
  static const String goodsDetail = "/goods/detail";

  ///商家端
  static const String store = "/store";

  ///商家订单
  static const String storeOrder = "/store/order";

  ///店铺订单列表
  static const String storeOrderList = "/store/order/list";

  ///商家订单详情
  static const String storeOrderDetail = "/store/order/detail";

  ///店铺营收
  static const String storeRevenue = "/store/revenue";

  ///商家账单详情
  static const String storeBillDetail = "/store/bill/detail";

  ///商家提现记录
  static const String storeWithdrawRecord = "/store/withdraw/record";

  ///设置页面
  static const String setting = "/setting";

  ///别名映射页面
  static final List<GetPage> getPages = [
    GetPage(
      name: index,
      page: () => SplashPage(),
      binding: SplashBinding(),
    ),
    GetPage(
      name: login,
      page: () => LoginPage(),
      binding: LoginBinding(),
    ),
    GetPage(
      name: main,
      page: () => MainPage(),
      binding: MainBinding(),
    ),
    GetPage(
      name: goodsDetail,
      page: () => DetailPage(),
      binding: DetailBinding(),
    ),
    GetPage(
      name: store,
      page: () => StorePage(),
      binding: StoreBinding(),
    ),
    GetPage(
      name: storeOrder,
      page: () => StoreOrderPage(),
      binding: StoreOrderBinding(),
    ),
    GetPage(
      name: storeOrderDetail,
      page: () => StoreOrderDetailPage(),
      binding: StoreOrderDetailBinding(),
    ),
    GetPage(
      name: storeRevenue,
      page: () => StoreRevenuePage(),
      binding: StoreRevenueBinding(),
    ),
    GetPage(
      name: storeBillDetail,
      page: () => BillDetailPage(),
      binding: BillDetailBinding(),
    ),
    GetPage(
      name: storeWithdrawRecord,
      page: () => WithdrawRecordPage(),
      binding: WithdrawRecordBinding(),
    ),
    GetPage(
      name: setting,
      page: () => SettingPage(),
      binding: SettingBinding(),
    ),
  ];
}
