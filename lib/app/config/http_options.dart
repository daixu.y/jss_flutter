import 'package:flutter/material.dart';

// 请求配置
class HttpOptions {
  // 连接服务器超时时间，单位是毫秒
  static const int CONNECT_TIMEOUT = 30000;
  // 接收超时时间，单位是毫秒
  static const int RECEIVE_TIMEOUT = 30000;
  // 地址前缀
  // static const String BASE_URL = 'http://dev.duuchin.com';
  static const BASE_URL_NO_API = "https://jss.test.zjjushuashua.com/";

  static const String BASE_URL = BASE_URL_NO_API + 'api/';

  // static const String BASE_TOKEN = 'Bearer 19800019176:03c3d29e143b4f9cb4c069e7b98368b5';

  static const String BASE_TOKEN = '';
}